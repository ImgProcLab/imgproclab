package hu.uni_pannon.mik.mat.ipf.shared;

import java.util.Set;

import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;

public interface IPFSharedDataComponents extends IPFSharedResources, IPFSharedComponents {
	public enum DataFields {
		Type;
	}

	public enum AttributeType {
		attTypeValue, attTypeObject, attTypeArray, attTypeNull
	}

	public enum ValueType {
		attString(String.class), attBool(Boolean.class), attLong(Long.class), attDouble(Double.class), attNull();

		private ValueType(Class<?>... contentClass) {
			if (!IPFUtilsBasic.arrIsEmpty(contentClass)) {
				this.contentClass = contentClass[0];
				for (Class<?> cc : contentClass) {
					IPFSharedUtilsInternal.valTypeRevMap.put(cc, this);
				}
			} else {
				this.contentClass = null;
			}
		}

		public final Class<?> contentClass;

		public static ValueType fromObject(Object o) {
			return (null == o ) ? ValueType.attNull : IPFSharedUtilsInternal.valTypeRevMap.get(o.getClass());
		}

		public String valueToString(Object value) {
			String ret = null;

			if (null != value) {
				switch (this) {
				case attBool:
				case attDouble:
				case attLong:
					ret = value.toString();
					break;
				case attString:
					ret = (String) value;
					break;
				default:
					break;
				}
			}
			return ret;
		}

		@SuppressWarnings("unchecked")
		public <T> T stringToValue(String string) {
			Object ret = null;
			if (null != string) {
				switch (this) {
				case attBool:
					ret = Boolean.parseBoolean(string);
					break;
				case attDouble:
					ret = Double.parseDouble(string);
					break;
				case attLong:
					ret = Long.parseLong(string);
					break;
				case attString:
					ret = string;
					break;
				default:
					break;
				}
			}
			return (T) ret;
		}
	}

	public enum ListenerOp {
		add, remove, test, clear
	}

	public static interface DataChangeEvent {
		DataObject getObject();

		String getAttrName();
	}

	public static interface DataChangeListener {
		void dataChanged(DataChangeEvent event);
	}

	public static interface DataProcessor<T> {
		void processStart(Object key) throws Exception;

		ProcessResult processData(Object key, T data) throws Exception;

		void processEnd(Object key, ProcessResult result) throws Exception;
	}

	public static abstract class DataProcessorDefault<T> implements DataProcessor<T> {
		@Override
		public void processStart(Object key) throws Exception {
		}

		@Override
		public ProcessResult processData(Object key, T data) throws Exception {
			return ProcessResult.skipped;
		}

		@Override
		public void processEnd(Object key, ProcessResult result) throws Exception {
		}
	}

	public static interface DataObject {
		String getTypeName();

		Iterable<String> getAttributeNames();

		<T> T getAttribute(Object key);

		AttributeType getAttributeType(Object key);

		ValueType getValueType(Object key);

		<T> ProcessResult iterateAttribute(Object key, boolean skipNulls, DataProcessor<T>... dp) throws Exception;

		<T> ProcessResult browseTree(Set<DataObject> seen, DataProcessor<DataObject> dp) throws Exception;

		void removeAttribute(Object key, int... index);

		void setAttribute(Object key, Object value);

		void setAttribute(Object key, int index, Object... value);

		boolean setListener(ListenerOp op, DataChangeListener listener);
	}

	public static class DataObjectWrapper implements HasId {
		protected final DataObject wrapped;

		public DataObjectWrapper(DataObject wrapped) {
			this.wrapped = wrapped;
		}

		@Override
		public String getId() {
			return wrapped.getAttribute(GenericFields.id);
		}
	}
	
	interface DataObjectFormatter {
		String nodeToString(DataObject node);
	}
	
	interface DataObjectController extends DataObjectFormatter {
		boolean browseChildren(DataObject node);
	}

	interface EditController {
		boolean isEditable();
	}

}
