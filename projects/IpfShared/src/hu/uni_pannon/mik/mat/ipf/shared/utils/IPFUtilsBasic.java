package hu.uni_pannon.mik.mat.ipf.shared.utils;

import java.util.HashMap;
import java.util.Map;

import hu.uni_pannon.mik.mat.ipf.shared.IPFBasicComponents;


public class IPFUtilsBasic implements IPFBasicComponents {
	public static boolean strIsEmpty(String str) {
		return (null == str) ? true : 0 == str.trim().length();
	}


	public static boolean safeIsEqual(Object o1, Object o2) {
		return (null == o1) ? (null == o2) ? true : false : (null == o2) ? false : o1.equals(o2);
	}

	public static String safeToStr(Object ob) {
		return safeToStr(ob, "");
	}

	public static String safeToStr(Object ob, String defStr) {
		return (null == ob) ? defStr : ob.toString();
	}

	public static String buildStr(Object... content) {
		return buildStr(PATH_SEPARATOR, content);
	}

	public static String buildStr(String sep, Object... content) {
		StringBuilder sb = null;

		if ((null != content) && (0 < content.length)) {
			for (Object o : content) {
				if (null != o) {
					if ( null == sb ) {
						sb = new StringBuilder();
					} else if ( null != sep ) {
						sb.append(sep);
					}
					sb.append(o);
				}
			}
		}
		
		return (null == sb) ? "" : sb.toString();
	}
	
	public static int indexOf(Object item, Object...objects) {
		int l;
		
		if ((null != objects) && (0 < (l = objects.length)) ) {
			for ( int i = 0; i < l; ++i ) {
				if ( safeIsEqual(item, objects[i])) {
					return i;
				}
			}
		}
		
		return NOT_FOUND_INDEX;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	private static IPFUtilsPool<Class<?>, Map<Object, Enum<?>>> enumPool = new IPFUtilsPool<Class<?>, Map<Object,Enum<?>>>() {
		@Override
		protected Map<Object, Enum<?>> create(Class<?> key) throws Exception {
			Map m = new HashMap<Object, Enum<?>>();
			for ( Object c : key.getEnumConstants() ) {
				Enum e = (Enum) c;
				m.put(e.name(), e);
			}

			return m;
		}
	};
	
	@SuppressWarnings("unchecked")
	public static <T extends Enum<T>> T enumForOb(Object ob, Class<T> enumClass, T defValue) {
		T ret = (T) enumPool.get(enumClass).get(ob);
		return (null == ret) ? defValue : ret;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Enum<T>> Map<T, String> getSysParams(Class<T> enumClass) {
		Map<T, String> ret = new HashMap<T, String>();
		for ( Enum<?> key : enumPool.get(enumClass).values()) {
			String val = System.getProperty(key.name());
			if ( !strIsEmpty(val) ) {
				ret.put((T) key, val);
			}
		}
		
		return ret;
	}


	public static boolean arrIsEmpty(Object[] arr) {
		return (null == arr) || (0 == arr.length);
	}
}