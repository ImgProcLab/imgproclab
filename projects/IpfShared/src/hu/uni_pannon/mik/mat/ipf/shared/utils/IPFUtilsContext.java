package hu.uni_pannon.mik.mat.ipf.shared.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.ProcessContext;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;

public class IPFUtilsContext implements IPFSharedComponents, ProcessContext {
	Map<String, Object> content = new HashMap<String, Object>();
	
	@Override
	public Integer getCurrentStep() {
		return 0;
	}
		
	@Override
	public Iterable<String> getIds(boolean includePrevious, Integer step) {
		return content.keySet();
	}

	@Override
	public void readStream(String id, StreamReader reader, Integer step) throws Exception {
		byte[] data = (byte[]) content.get(id);

		if (null != data) {
			InputStream is = null;
			try {
				is = new ByteArrayInputStream(data);
				reader.loadStream(is);
			} finally {
				if (null != is) {
					is.close();
				}
			}
		}
	}

	@Override
	public void writeStream(String id, StreamWriter writer) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			writer.writeStream(os);
		} finally {
			if (null != os) {
				os.close();
			}
		}

		content.put(id, os.toByteArray());
	}

	@Override
	public DataObject getData(String id, Integer step) throws Exception {
		return (DataObject) content.get(id);
	}

	@Override
	public void setData(String id, DataObject data) throws Exception {
		content.put(id,  data);
	}

	@Override
	public void remove(String id) throws Exception {
		content.remove(id);
	}
}
