package hu.uni_pannon.mik.mat.ipf.shared.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.GenericEvents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.Resetable;

public abstract class IPFUtilsPool<TKey, TValue> implements Resetable {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public enum PoolType {
		weak(WeakHashMap.class), sorted(TreeMap.class), normal(HashMap.class);

		final Class<? extends Map> mapClass;

		private PoolType(Class<? extends Map> mapClass) {
			this.mapClass = mapClass;
		}

		<K, V> Map<K, V> createMap() {
			Map<K, V> ret = null;

			try {
				ret = mapClass.newInstance();
			} catch (Exception e) {
				IPFUtilsCommon.safeSwallowException(e, GenericEvents.totallyUnexpectedException, true);
			}

			return ret;
		}
	}

	final IPFUtilsPool.PoolType poolType;
	final Object creationError;
	final TValue defaultItem;

	Map<TKey, TValue> content;

	public IPFUtilsPool() {
		this(PoolType.normal, GenericEvents.poolItemCreationException, null);
	}

	public IPFUtilsPool(IPFUtilsPool.PoolType poolType, Object creationError, TValue defaultItem) {
		this.poolType = poolType;
		this.creationError = creationError;
		this.defaultItem = defaultItem;
	}

	public TValue peek(TKey key) {
		synchronized (poolType) {
			return (null == content) ? null : content.get(key);
		}
	}

	public TValue get(TKey key) {
		TValue ret;

		synchronized (poolType) {
			if (null == content) {
				content = new HashMap<>();
				ret = null;
			} else {
				ret = content.get(key);
			}

			if (null == ret) {
				try {
					ret = create(key);
				} catch (Exception e) {
					IPFUtilsCommon.safeSwallowException(e, creationError, null == defaultItem);
					ret = defaultItem;
				}
				content.put(key, ret);
			}
		}

		return ret;
	}

	@Override
	public void reset() {
		synchronized (poolType) {
			if (null != content) {
				content.clear();
			}
		}
	}

	protected abstract TValue create(TKey key) throws Exception;
}