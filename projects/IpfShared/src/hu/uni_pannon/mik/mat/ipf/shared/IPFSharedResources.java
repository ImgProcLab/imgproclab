package hu.uni_pannon.mik.mat.ipf.shared;



public interface IPFSharedResources {
	enum SharedResIds {
		apptitle("PE-MIK Image Processing Framework");
				
		private SharedResIds(String resText) {
			this.resText = resText;
		}

		final String resText;
		
		@Override
		public String toString() {
			return resText;
		}
	}
	
	
}
