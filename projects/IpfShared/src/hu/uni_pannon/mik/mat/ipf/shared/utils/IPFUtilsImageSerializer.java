package hu.uni_pannon.mik.mat.ipf.shared.utils;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.ProcessContext;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.ProcessResult;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.StreamReader;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.StreamWriter;

public class IPFUtilsImageSerializer implements StreamReader, StreamWriter {
	BufferedImage img;
	String formatName = "BMP";

	@Override
	public ProcessResult loadStream(InputStream is) throws Exception {
		img = ImageIO.read(is);
		return ProcessResult.finished;
	}

	@Override
	public ProcessResult writeStream(OutputStream os) throws Exception {
		ImageIO.write(img, formatName, os);
		return ProcessResult.finished;
	}

	public BufferedImage getImg() {
		return img;
	}

	public void setImg(BufferedImage img) {
		this.img = img;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	public static BufferedImage readImage(ProcessContext ctx, String name) throws Exception {
		IPFUtilsImageSerializer ir = new IPFUtilsImageSerializer();

		ctx.readStream(name, ir, null);

		return ir.getImg();
	}

	public static void writeImage(ProcessContext ctx, BufferedImage img, String name) throws Exception {
		IPFUtilsImageSerializer ir = new IPFUtilsImageSerializer();
		ir.setImg(img);
		ctx.writeStream(name, ir);
	}

}
