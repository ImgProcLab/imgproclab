package hu.uni_pannon.mik.mat.ipf.shared.utils;

import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.DataObjectFactory;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.LogicFactory;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents;

public class IPFUtilsMain implements IPFSharedComponents, IPFSharedDataComponents {
	enum LaunchEvents {
		missingReferredPackage, missingStreamChannel
	}

	enum LaunchParams {
		cloudImpl, dataFactoryImpl, dataSourceImpl, logicFactoryImpl
	}

	enum MainParams {
		StreamChannels, Packages, MainComponent
	}

	public static void main(String[] args) throws Exception {
		launch(args);
	}

	public static void loadConfig(DataObject app, final IPFSharedCloud cloud, final Configurable target, Object attrKey, final Enum<?> failureMsg) throws Exception {
		app.iterateAttribute(attrKey, true, new DataProcessorDefault<String>() {
			@Override
			public ProcessResult processData(Object key, String data) throws Exception {

				DataObject param = cloud.load(data);
				if (null != param) {
					target.loadConfig(param);
					return ProcessResult.processed;
				}
				
				IPFUtilsCommon.broadcastEvent(failureMsg, data);
				return ProcessResult.skipped;
			}
		});
	}

	public static void launch(String[] args) throws Exception {		
		InputStream pis = ClassLoader.getSystemResourceAsStream("ipfLaunch.properties");

		if ( null != pis ) {
			Properties p = new Properties();
			p.load(pis);
			pis.close();
			
			for ( LaunchParams lp1 : LaunchParams.values() ) {
				String n = lp1.name();
				String prop = p.getProperty(n);
				
				if ( null == System.getProperty(n) ) {
					System.setProperty(n, prop);
				}
			}
			System.out.println("Properties loaded");
		} else {
			System.out.println("Properties not loaded");
		}
		
		Map<LaunchParams, String> lp = IPFUtilsCommon.getSysParams(LaunchParams.class);		
		System.out.println("LP size: " + lp.size());


		final LogicFactory lf = (LogicFactory) Class.forName(lp.get(LaunchParams.logicFactoryImpl)).newInstance();
		final IPFSharedCloud cloud = (IPFSharedCloud) Class.forName(lp.get(LaunchParams.cloudImpl)).newInstance();

		final Configurable dataSource = (Configurable) Class.forName(lp.get(LaunchParams.dataSourceImpl)).newInstance();
		final DataObjectFactory df = (DataObjectFactory) Class.forName(lp.get(LaunchParams.dataFactoryImpl)).newInstance();
		df.setSource(dataSource);

		cloud.init(df, lf);
		
		IPFUtilsCommon.init(cloud);

		String appFile = "default.ipfa";
		DataObject app = cloud.load(appFile);

		if (null != app) {
			loadConfig(app, cloud, dataSource, MainParams.StreamChannels, LaunchEvents.missingStreamChannel);
			loadConfig(app, cloud, lf, MainParams.Packages, LaunchEvents.missingReferredPackage);
			
			DataObject main = app.getAttribute(MainParams.MainComponent);
			cloud.send(main, null);
		}
	}

}
