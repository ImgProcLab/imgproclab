package hu.uni_pannon.mik.mat.ipf.shared;

import java.io.InputStream;
import java.io.OutputStream;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataProcessor;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.Logic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public interface IPFSharedCloud extends IPFSharedComponents {
	public enum CloudEvents {
		logicCreationException, missingLogicCalled, missingLogicInited, friendAccessFailed, invalidLogicClass, messageProcessFailed
	}
	
	Logic NO_LOGIC = new Logic() {
		public void release() throws Exception {
		}
		public ProcessResult process(DataObject message) throws Exception {
			IPFUtilsCommon.broadcastEvent(CloudEvents.missingLogicCalled, message);
			return ProcessResult.skipped;
		}
		public void init(DataObject self) throws Exception {
			IPFUtilsCommon.broadcastEvent(CloudEvents.missingLogicInited, self);
		}
	};

		
	public static interface DataObjectFactory {
		void setSource(Object source);
		
		DataObject createObject(Object type);

		DataObject load(Object sourceId) throws Exception;
		void save(Object targetId, DataObject data) throws Exception;
		DataObject load(InputStream is) throws Exception;
		void save(OutputStream os, DataObject data) throws Exception;
	}

	public static interface LogicFactory extends Configurable {
		Logic createLogic(Object type) throws Exception;
	}

	public static interface StreamFactory extends Configurable {
		InputStream getInputStream(String id, int step) throws Exception;
		OutputStream getOutputStream(String id, int step) throws Exception;
		void removeStream(String id, int step) throws Exception;
	}

	
	public enum CloudMessages {
		loadPackage,
	}
	
	public enum StreamFactoryMessages {
		getInputStream, getOutputStream
	}
	
	public enum CloudFields {
		Type,
	}
	
	public enum LogicFactoryFields {
		LogicAssignments, implClass, packageId
	}

	void init(DataObjectFactory dataFactory, LogicFactory logicFactory);
	
	DataObject load(Object source) throws Exception;
	void store(DataObject ob, Object source) throws Exception;
	
	DataObject createObject(Object type);
	
	ProcessResult iterateByType(Object type, DataProcessor<DataObject> processor) throws Exception;
	DataObject getObject(Object id);
	ProcessResult send(DataObject target, DataObject message) throws Exception;
	
	<T> T getWrappedObject(DataObject ob);
	
}
