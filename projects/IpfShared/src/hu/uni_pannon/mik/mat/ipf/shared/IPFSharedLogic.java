package hu.uni_pannon.mik.mat.ipf.shared;

import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCmdLoader;


public interface IPFSharedLogic extends IPFSharedDataComponents {

	public enum LogicMessages {
	}

	public enum MessageFields {
		command, Context
	}
	
	public enum SimppleLogicMessages {
		execute,
	}


	public static interface Logic {
		void init(DataObject self) throws Exception;

		ProcessResult process(DataObject message) throws Exception;

		void release() throws Exception;
	}
	
	public static abstract class LogicDefault implements Logic {
		private DataObject self;
		IPFUtilsCmdLoader loader;
		
		@SafeVarargs
		protected LogicDefault(Class<? extends Enum<?>>... apis) {
			loader = new IPFUtilsCmdLoader(apis);
		}

		@Override
		public void init(DataObject self) throws Exception {
			this.self = self;
		}
		
		@Override
		public void release() throws Exception {
		}

		@Override
		public ProcessResult process(DataObject message) throws Exception {
			return process(loader.resolveCmd(message, null), message);
		}
		
		protected final DataObject getSelf() {
			return self;
		}
		
		protected abstract ProcessResult process(CmdCall cmd, DataObject param) throws Exception;
	}
	
	public static abstract class ImageProcessorBase extends LogicDefault implements ImageProcessor {
		
		public ImageProcessorBase() {
			super(SimppleLogicMessages.class);
		}
		
		@Override
		protected ProcessResult process(CmdCall cmd, DataObject param) throws Exception {
			ProcessContext pctx = param.getAttribute(MessageFields.Context);
			return execute(pctx);
		}		
	}
	
	public static abstract class LogicWrapper<WrappedType> extends LogicDefault {
		private WrappedType wrappedObject;
		
		public final WrappedType getWrapped() {
			return wrappedObject;
		}

		@Override
		public void init(DataObject self) throws Exception {
			super.init(self);
			wrappedObject = createWrapped(self);
		}
		protected WrappedType createWrapped(DataObject self) throws Exception {
			return null;
		}

	}

	public interface CmdCall {
		public abstract Enum<?> getCmd();
		public abstract int getCmdIdx();
		public abstract Class<? extends Enum<?>> getCmdSource();
	}

}
