package hu.uni_pannon.mik.mat.ipf.shared;

import java.io.InputStream;
import java.io.OutputStream;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;

public interface IPFSharedComponents extends IPFSharedResources, IPFBasicComponents {
//	String defaultImage = "testpattern.jpg";
	String defaultImage = "input/smiley.jpeg";

	public enum GenericEvents {
		poolItemCreationException, totallyUnexpectedException, cmdExecutionException
	}

	public enum GenericFields {
		command, Parameter, name, id, result, Parent, path, Input, Output, Image, Members, regexp
	}


	public enum ProcessResult {
		aborted(true, false), failed(true, false), finished(true, true), skipped(false, false), processed(false, true);

		public final boolean stop;
		public final boolean success;

		private ProcessResult(boolean stop, boolean success) {
			this.stop = stop;
			this.success = success;
		}
	}

	public interface HasId {
		String getId();
	}

	public interface Resetable {
		void reset();
	}

	public static class SwallowedException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public SwallowedException(Throwable source) {
			super("Swallowed exception", source);
		}
	}

	public static interface Configurable {
		void loadConfig(DataObject config) throws Exception;
	};

	public static interface StreamReader {
		ProcessResult loadStream(InputStream is) throws Exception;
	};

	public static interface StreamWriter {
		ProcessResult writeStream(OutputStream os) throws Exception;
	};

	public static interface ProcessContext {
		Integer getCurrentStep();
		Iterable<String> getIds(boolean includePrevious, Integer step);
		void readStream(String id, StreamReader reader, Integer step) throws Exception;

		void writeStream(String id, StreamWriter writer) throws Exception;

		DataObject getData(String id, Integer step) throws Exception;

		void setData(String id, DataObject data) throws Exception;

		void remove(String id) throws Exception;
	};

	public static interface ImageProcessor {
		ProcessResult execute(ProcessContext context) throws Exception;
	}

	//
	// public static class Test {
	// public void test() {
	// ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage();
	// }
	// }
}
