package hu.uni_pannon.mik.mat.ipf.shared.utils;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.GenericFields;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.CmdCall;

public class IPFUtilsCmdLoader {
	public static class CmdCallImpl implements CmdCall {
		int cmdIdx;
		Class<? extends Enum<?>> cmdSource;
		Enum<?> cmd;
		
		@Override
		public Enum<?> getCmd() {
			return cmd;
		}
		
		@Override
		public int getCmdIdx() {
			return cmdIdx;
		}
		
		@Override
		public Class<? extends Enum<?>> getCmdSource() {
			return cmdSource;
		}
	}

	public final Class<? extends Enum<?>>[] cmds;

	@SuppressWarnings("unchecked")
	public IPFUtilsCmdLoader(Class<? extends Enum<?>>... cmds) {
		this.cmds = cmds;
	}
	
	public CmdCall resolveCmd(DataObject msg, CmdCall item) {
		CmdCallImpl cci = ( null == item ) ? new CmdCallImpl() : (CmdCallImpl) item;
		
		Object val = msg.getAttribute(GenericFields.command);
		
		if ( val instanceof Enum) {
			cci.cmdIdx = IPFUtilsBasic.indexOf(val.getClass(), (Object[]) cmds);
			cci.cmdSource = cmds[cci.cmdIdx];
			cci.cmd = (Enum<?>) val;
		}
		
		return cci;
	}
}