package hu.uni_pannon.mik.mat.ipf.shared;


public interface IPFBasicComponents extends IPFSharedResources {
	String PATH_SEPARATOR = ".";
	String PATH_SPLIT = "\\.";

	String SPEC_FIELD_PREFIX = "!";

	String SPEC_FLD_TYPE = SPEC_FIELD_PREFIX + "type";
	String SPEC_FLD_TYPEOB = SPEC_FIELD_PREFIX + "typeOb";
	String SPEC_FLD_ID = SPEC_FIELD_PREFIX + "id";

	int NOT_FOUND_INDEX = -1;
}
