package hu.uni_pannon.mik.mat.ipf.shared.utils;

import java.util.HashMap;
import java.util.Map;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.CloudEvents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;

public class IPFUtilsCommon extends IPFUtilsBasic implements IPFSharedComponents, IPFSharedDataComponents {
	private static final Map<Class<?>, ValueType> classToType = new HashMap<Class<?>, ValueType>();

	static {
		for (ValueType at : ValueType.values()) {
			classToType.put(at.contentClass, at);
		}
	}

	// Warning: fails if id is not Enum or String!
	public static String toStrKey(Object id) {
		return (id instanceof Enum) ? ((Enum<?>) id).name() : (String) id;
	}

	public static String getResText(Enum<?> resId) {
		return resId.toString();
	}

	// WARNING - may throw a runtimeException, if the error level is fatal! Use
	// try {} finally {} if you have open resources or state...
	// TODO - handle event severity
	public static void broadcastEvent(Object eventId, Object ob) {
		System.err.println(buildStr(": ", "Event", eventId, ob));
	}

	public static void safeSwallowException(Throwable t, Object errMsg, boolean rethrow) {
		if (!(t instanceof SwallowedException)) {
			System.err.println(buildStr(": ", "Exception", errMsg, t));
			t = new SwallowedException(t);
		}
		if (rethrow) {
			throw (RuntimeException) t;
		}
	}

	public static ValueType getObjectAttType(Object ob) {
		return (null == ob) ? null : classToType.get(ob.getClass());
	}

	public static <T> T getAttrByPathStr(DataObject data, String path) {
		return getAttribute(data, (Object[]) path.split(PATH_SPLIT));
	}

	@SuppressWarnings("unchecked")
	public static <T> T getAttribute(DataObject data, Object... path) {
		Object d = data;

		for (Object p : path) {
			if (!(d instanceof DataObject)) {
				return null;
			}
			d = ((DataObject) d).getAttribute(p.toString());
		}

		return (T) d;
	}

	private static IPFSharedCloud cloud;

	public static void init(IPFSharedCloud cloud_) {
		if (null == cloud) {
			cloud = cloud_;
		}
	}

	public static DataObject createDataObject(Object type) {
		return cloud.createObject(type);
	}

	public static DataObject getRootObject(Object id) {
		return cloud.getObject(id);
	}

	public static ProcessResult searchRootObjects(Object type, DataProcessor<DataObject> processor) throws Exception {
		return cloud.iterateByType(type, processor);
	}

	public static ProcessResult send(DataObject target, DataObject message) {
		try {
			return cloud.send(target, message);
		} catch (Exception e) {
			safeSwallowException(e, CloudEvents.messageProcessFailed, false);
			return ProcessResult.failed;
		}
	}

	public static <T> T getWrappedObject(DataObject ob) {
		return cloud.getWrappedObject(ob);
	}

	public static <T extends Enum<T>> T enumForAttr(DataObject data, Object key, Class<T> enumClass, T defValue) {
		Object val = data.getAttribute(key);
		return (null == val) ? null : enumForOb(val, enumClass, defValue);
	}

	public static boolean isArrOK(Object... values) {
		return (null != values) && (0 < values.length);
	}

	public static <T> T getAttDefVal(DataObject data, String key, T defValue) {
		T ret = data.getAttribute(key);
		return (null == ret) ? defValue : ret;
	}

	public static DataObject createObjectByType(DataObject obType)  {
		DataObject ret = IPFUtilsCommon.createDataObject(obType.getAttribute(SPEC_FLD_ID));
		ret.setAttribute(SPEC_FLD_TYPEOB, obType);

		DataObject params = obType.getAttribute("Parameters");

		if (null != params) {
			for (String pn : params.getAttributeNames()) {
				DataObject p = params.getAttribute(pn);

				String defValue = p.getAttribute("defValue");
				if (!IPFUtilsBasic.strIsEmpty(defValue)) {
					ValueType vt = IPFUtilsCommon.enumForAttr(p, "valueType", ValueType.class, ValueType.attString);
					ret.setAttribute(pn, vt.stringToValue(defValue));
				}
			}
		}

		return ret;
	}

	public static void store(DataObject ob, String fName) throws Exception {
		cloud.store(ob, fName);		
	}

}