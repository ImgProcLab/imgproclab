import org.bytedeco.javacpp.opencv_core.DMatchVector;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point2f;
import org.bytedeco.javacpp.opencv_features2d;
import org.bytedeco.javacpp.opencv_imgcodecs;
import org.bytedeco.javacpp.opencv_imgproc;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

public class OrbMatchDemo {

    public static void main (String[] args) throws Exception {
        String sourceFile = args.length >= 1 ? args[0] : "Lenna.png";

        Mat source = opencv_imgcodecs.imread(sourceFile);
        show(source, sourceFile);

        opencv_features2d.ORB orb =
            opencv_features2d.ORB.create(500, 1.2f, 8, 31, 0, 2, opencv_features2d.ORB.HARRIS_SCORE, 31, 20);

        KeyPointVector sourceKeypoints = new KeyPointVector();
        orb.detect(source, sourceKeypoints);
        showKeypoints(source, sourceKeypoints, "Features");

        Mat sourceDescriptors = new Mat();
        orb.compute(source, sourceKeypoints, sourceDescriptors);

        Mat transformed = new Mat();
        Point2f center = new Point2f(source.cols() / 2, source.rows() / 2);
        Mat matrix = opencv_imgproc.getRotationMatrix2D(center, -45, 0.5);
        opencv_imgproc.warpAffine(source, transformed, matrix, source.size());
        show(transformed, "Transformed");

        KeyPointVector transformedKeypoints = new KeyPointVector();
        orb.detect(transformed, transformedKeypoints);
        showKeypoints(transformed, transformedKeypoints, "Transformed features");

        Mat transformedDescriptors = new Mat();
        orb.compute(transformed, transformedKeypoints, transformedDescriptors);

        DMatchVector matches = new DMatchVector();
        opencv_features2d.BFMatcher matcher = new opencv_features2d.BFMatcher();
        matcher.match(transformedDescriptors, sourceDescriptors, matches);

        Mat dst = new Mat();
        opencv_features2d.drawMatches(source, sourceKeypoints, transformed, transformedKeypoints, matches, dst);
        show(dst, "Matches");
    }

    public static void showKeypoints (Mat image, KeyPointVector keypoints, String caption) throws Exception {
        try( Mat dst = new Mat() ){
            opencv_features2d.drawKeypoints(image, keypoints, dst);
            show(dst, caption);
        }
    }

    public static void show (Mat image, String caption) {
        Frame frame = new OpenCVFrameConverter.ToMat().convert(image);
        show(frame, caption);
    }

    private static void show (Frame frame, String caption) {
        CanvasFrame canvas = createCanvas(caption, frame.imageWidth, frame.imageHeight);
        canvas.showImage(frame);
    }

    private static CanvasFrame createCanvas (String caption, int width, int height) {
        CanvasFrame canvas = new CanvasFrame(caption, 1);
        canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        canvas.setCanvasSize(width, height);
        return canvas;
    }

}
