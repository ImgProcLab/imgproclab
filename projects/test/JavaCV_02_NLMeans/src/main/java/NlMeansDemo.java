import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_imgcodecs;
import org.bytedeco.javacpp.opencv_photo;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

public class NlMeansDemo {

    public static void main (String[] args) {
        String sourceFile = args.length >= 1 ? args[0] : "Lenna.png";

        Mat src = opencv_imgcodecs.imread(sourceFile);
        show(src, sourceFile);

        Mat dst = new Mat();
        opencv_photo.fastNlMeansDenoisingColored(src, dst, 3, 3, 7, 21);
        show(dst, sourceFile + " denoised");
    }

    public static void show (IplImage image, String caption) {
        Frame frame = new OpenCVFrameConverter.ToIplImage().convert(image);
        show(frame, caption);
    }

    public static void show (Mat image, String caption) {
        Frame frame = new OpenCVFrameConverter.ToMat().convert(image);
        show(frame, caption);
    }

    private static void show (Frame frame, String caption) {
        CanvasFrame canvas = createCanvas(caption, frame.imageWidth, frame.imageHeight);
        canvas.showImage(frame);
    }

    private static CanvasFrame createCanvas (String caption, int width, int height) {
        CanvasFrame canvas = new CanvasFrame(caption, 1);
        canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        canvas.setCanvasSize(width, height);
        return canvas;
    }

}
