import javax.swing.WindowConstants;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_video;
import org.bytedeco.javacpp.opencv_video.BackgroundSubtractorMOG2;
import org.bytedeco.javacpp.opencv_videoio.VideoCapture;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

public class BackgroundSubtractorDemo {

    public static void main (String[] args) throws Exception {
        String file = args.length >= 1 ? args[0] : "768x576.avi";

        try( VideoCapture video = new VideoCapture(file) ){
            try( BackgroundSubtractorMOG2 subtractor = opencv_video.createBackgroundSubtractorMOG2(1000, 16, true) ){
                CanvasFrame frameCanvas = createCanvas(file);
                CanvasFrame backgroundCanvas = createCanvas("Background");
                while( video.grab() ){
                    try( Mat frame = new Mat() ){
                        video.retrieve(frame);
                        try( Mat foreGroundMask = new Mat() ){
                            subtractor.apply(frame, foreGroundMask);
                        }
                        try( Mat background = new Mat() ){
                            subtractor.getBackgroundImage(background);
                            show(frameCanvas, frame);
                            show(backgroundCanvas, background);
                        }
                    }
                }
            }
        }
    }

    private static CanvasFrame createCanvas (String caption) {
        CanvasFrame canvas = new CanvasFrame(caption, 1);
        canvas.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        return canvas;
    }

    private static void show (CanvasFrame canvas, Mat image) {
        Frame frame = new OpenCVFrameConverter.ToMat().convert(image);
        canvas.setSize(frame.imageWidth, frame.imageHeight);
        canvas.showImage(frame);
    }

}
