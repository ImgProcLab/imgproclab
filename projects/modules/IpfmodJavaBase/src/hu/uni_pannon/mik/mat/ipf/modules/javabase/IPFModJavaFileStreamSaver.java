package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Pattern;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.ProcessContext;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.ProcessResult;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.StreamReader;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.ImageProcessorBase;

public class IPFModJavaFileStreamSaver extends ImageProcessorBase {
	String file;
	String regex;
	Integer step;
	boolean includePrevious;
	ArrayList<String> include;
	ArrayList<String> exclude;
	
	Path root;
	Path currPath;

	StreamReader fw = new StreamReader() {
		@Override
		public ProcessResult loadStream(InputStream is) throws Exception {
			Files.copy(is, currPath);
			return ProcessResult.finished;
		}
	};

	public void sendFile(ProcessContext context, String id) throws Exception {
		if ( null == root ) {
			File f = new File(file);
			f.mkdirs();
			root = f.toPath();
		}
		
		currPath = root.resolve(id);
		
		context.readStream(id, fw, null);
	};

	@Override
	public ProcessResult execute(ProcessContext context) throws Exception {
		ProcessResult ret = ProcessResult.skipped;

		Pattern p = (null == regex) ? null : Pattern.compile(regex);
		
		for ( String id : context.getIds(includePrevious, step) ) {
			if ((null == p) || p.matcher(id).matches()) {
				sendFile(context, id);
				ret = ProcessResult.finished;
			}
		}

		return ret;
	}
}
