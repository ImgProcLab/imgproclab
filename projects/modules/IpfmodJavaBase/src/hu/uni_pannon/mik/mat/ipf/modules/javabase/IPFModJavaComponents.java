package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents;

public interface IPFModJavaComponents extends IPFSharedComponents {
	public enum ProcessFields {
		angle, x, y, z, shapeType, filled,
	}

	public enum PathType {
		dot, line, closed, filled
	}

	public enum RectParamBlock {
		point, size, roundingSize, arcAngles
	}

	public enum RectParam {
		x(RectParamBlock.point,0), y(RectParamBlock.point,1), width(RectParamBlock.size,0), height(RectParamBlock.size,1), arcWidth(RectParamBlock.roundingSize,0), arcHeight(RectParamBlock.roundingSize,1), startAngle(RectParamBlock.arcAngles,0), arcAngle(RectParamBlock.arcAngles,1);
		
		final RectParamBlock block;
		final int idx;
		
		private RectParam(RectParamBlock block, int idx) {
			this.block = block;
			this.idx = idx;
		}
		
		
	}

	public enum RectType {
		regular, rounded(RectParamBlock.roundingSize), oval, arc(RectParamBlock.arcAngles);

		final RectParamBlock[] params;

		private RectType() {
			params = new RectParamBlock[] { RectParamBlock.point, RectParamBlock.size };
		}

		private RectType(RectParamBlock p) {
			params = new RectParamBlock[] { RectParamBlock.point, RectParamBlock.size, p };
		}
	}

}
