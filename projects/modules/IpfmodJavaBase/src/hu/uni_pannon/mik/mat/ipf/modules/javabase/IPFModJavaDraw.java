package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataProcessorDefault;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.ImageProcessorBase;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsPool;

public abstract class IPFModJavaDraw extends ImageProcessorBase implements IPFModJavaComponents {

	@Override
	public final ProcessResult execute(ProcessContext context) throws Exception {
		ProcessResult ret;
		DataObject self = getSelf();

		Rectangle2D bound = new Rectangle2D.Double();

		ret = load(self, context, bound);

		if (ProcessResult.processed == ret) {
			Rectangle r = bound.getBounds();

			BufferedImage img = new BufferedImage((int) r.getWidth(), (int) r.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D graph = (Graphics2D) img.getGraphics();

			ret = draw(self, context, graph);

			if (ret.success) {
				// save image
			}
		}

		return ret;
	}

	protected abstract ProcessResult load(DataObject self, ProcessContext context, Rectangle2D bound) throws Exception;

	protected abstract ProcessResult draw(DataObject self, ProcessContext context, Graphics2D graph) throws Exception;

	public static class RectShape extends IPFModJavaDraw {
		RectType rt;
		IPFUtilsPool<RectParamBlock, ArrayList<Double>> params = new IPFUtilsPool<RectParamBlock, ArrayList<Double>>() {
			@Override
			protected ArrayList<Double> create(RectParamBlock key) throws Exception {
				return new ArrayList<>();
			}
		};

		RectParamBlock readingRp;
		boolean filled;
		
		int getValue(RectParam p) {
			return params.get(p.block).get(p.idx).intValue();
		}
		
		@SuppressWarnings("unchecked")
		@Override
		protected ProcessResult load(DataObject self, ProcessContext context, Rectangle2D bound) throws Exception {
			ProcessResult ret = ProcessResult.finished;
			
			rt = IPFUtilsCommon.enumForAttr(self, ProcessFields.shapeType, RectType.class, RectType.regular);
			filled = self.getAttribute(ProcessFields.filled);

			for (RectParamBlock rp : rt.params) {
				readingRp = rp;
				
				ret = self.iterateAttribute(rp.name(), true, null, new DataProcessorDefault<Double>() {
					@Override
					public ProcessResult processData(Object key, Double data) throws Exception {
						params.get(readingRp).add(data);
						return ProcessResult.processed;
					}
				});
			}
			
			bound.setRect(getValue(RectParam.x), getValue(RectParam.y), getValue(RectParam.width), getValue(RectParam.height));
			
			return ret;
		}

		@Override
		protected ProcessResult draw(DataObject self, ProcessContext context, Graphics2D graph) throws Exception {
			int x = getValue(RectParam.x);
			int y = getValue(RectParam.y);
			int w = getValue(RectParam.width);
			int h = getValue(RectParam.height);

			switch (rt) {
			case arc:
				int s = getValue(RectParam.startAngle);
				int a = getValue(RectParam.arcAngle);
				if ( filled ) {
					graph.fillArc(x, y, w, h, s, a);
				} else {
					graph.drawArc(x, y, w, h, s, a);
				}
				
				break;
			case oval:
				if ( filled ) {
					graph.fillOval(x, y, w, h);
				} else {
					graph.drawOval(x, y, w, h);
				}
				break;
			case regular:
				if ( filled ) {
					graph.fillRect(x, y, w, h);
				} else {
					graph.drawRect(x, y, w, h);
				}
				break;
			case rounded:
				int ah = getValue(RectParam.arcHeight);
				int aw = getValue(RectParam.arcWidth);
				if ( filled ) {
					graph.fillRoundRect(x, y, w, h, aw, ah);
				} else {
					graph.drawRoundRect(x, y, w, h, aw, ah);
				}
				break;
			default:
				break;
			}
			
			return ProcessResult.processed;
		}
	}

	public static class Path extends IPFModJavaDraw {
		ArrayList<Double> point = new ArrayList<>();
		ArrayList<Point2D> path = new ArrayList<>();

		@SuppressWarnings("unchecked")
		protected ProcessResult load(DataObject self, ProcessContext context, Rectangle2D bound) throws Exception {
			ProcessResult ret = ProcessResult.finished;
			ret = self.iterateAttribute(GenericFields.Members, true, null, new DataProcessorDefault<Double>() {
				@Override
				public void processStart(Object key) throws Exception {
					point.clear();
				}

				@Override
				public ProcessResult processData(Object key, Double data) throws Exception {
					point.add(data);
					return ProcessResult.processed;
				}

				@Override
				public void processEnd(Object key, ProcessResult result) throws Exception {
					Point2D.Double pt = new Point2D.Double(point.get(0), point.get(1));
					path.add(pt);
					IPFModJavaUtils.bound(bound, pt);
				}
			});

			return ret;
		}

		@Override
		protected ProcessResult draw(DataObject self, ProcessContext context, Graphics2D graph) throws Exception {

			PathType pathType = IPFUtilsCommon.enumForAttr(self, ProcessFields.shapeType, PathType.class, PathType.line);

			if (PathType.dot == pathType) {
				for (Point2D pt : path) {
					graph.drawRect((int) pt.getX() - 1, (int) pt.getY() - 1, 2, 2);
				}
			} else {
				int count = path.size();

				int[] x = new int[count];
				int[] y = new int[count];

				for (int i = 0; i < count; ++i) {
					Point2D pt = path.get(i);
					x[i] = (int) pt.getX();
					y[i] = (int) pt.getY();
				}

				switch (pathType) {
				case closed:
					graph.drawPolygon(x, y, count);
					break;
				case dot:
					break;
				case filled:
					graph.fillPolygon(x, y, count);
					break;
				case line:
					graph.drawPolyline(x, y, count);
					break;
				default:
					break;
				}
			}

			return ProcessResult.processed;
		}
	}

}
