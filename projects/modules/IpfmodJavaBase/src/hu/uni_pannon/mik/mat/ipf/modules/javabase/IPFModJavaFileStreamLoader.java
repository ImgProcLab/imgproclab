package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import java.io.File;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.regex.Pattern;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.ProcessContext;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.ProcessResult;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents.StreamWriter;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.ImageProcessorBase;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public class IPFModJavaFileStreamLoader extends ImageProcessorBase {
	String targetName;

	File currFile;

	StreamWriter fw = new StreamWriter() {
		@Override
		public ProcessResult writeStream(OutputStream os) throws Exception {
			Files.copy(currFile.toPath(), os);
			return ProcessResult.finished;
		}
	};

	public void sendFile(ProcessContext context, File f) throws Exception {
		currFile = f;
		context.writeStream(f.getName(), fw);
	};

	@Override
	public ProcessResult execute(ProcessContext context) throws Exception {
		ProcessResult ret = ProcessResult.skipped;

		DataObject self = getSelf();
		String file = IPFUtilsCommon.getAttDefVal(self, "file", ".");
		String regex = self.getAttribute("regexp");

		File fSource = new File(file);

		if (fSource.exists()) {
			if (fSource.isDirectory()) {
				Pattern p = (null == regex) ? null : Pattern.compile(regex);

				for (File f : fSource.listFiles()) {
					if (f.isFile() && ((null == p) || p.matcher(f.getName()).matches())) {
						sendFile(context, f);
						ret = ProcessResult.finished;
					}
				}
			} else {
				sendFile(context, fSource);
				ret = ProcessResult.finished;
			}
		}

		return ret;
	}
}
