package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataProcessor;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataProcessorDefault;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.ImageProcessorBase;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public class IPFModJavaChain extends ImageProcessorBase implements IPFModJavaComponents {
	DataObject msg = IPFUtilsCommon.createDataObject("msg");
	
	@Override
	public ProcessResult execute(ProcessContext context) throws Exception {
		DataObject self = getSelf();
		DataProcessor<DataObject> caller = new DataProcessorDefault<DataObject>(){
			@Override
			public ProcessResult processData(Object key, DataObject data) throws Exception {
				IPFUtilsCommon.send(data, msg);
				return super.processData(key, data);
			}
		};

		self.iterateAttribute("Members", true, caller);
		
		return ProcessResult.processed;
	}

}
