package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import java.awt.Image;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class IPFModJavaUtils implements IPFModJavaComponents {
	
	public static Point2D[] getCorners(Rectangle2D rect, Point2D[] corners) {
		double xi = rect.getMinX();
		double xa = rect.getMaxX();
		double yi = rect.getMinY();
		double ya = rect.getMaxY();
		
		if ( null == corners ) {
			corners = new Point2D[4];
			
			for ( int i = 0; i < 4; ++i ) {
				corners[i] = new Point2D.Double();
			}
		}
		
		corners[0].setLocation(xi,yi);
		corners[1].setLocation(xa,yi);
		corners[2].setLocation(xi,ya);
		corners[3].setLocation(xa,ya);

		return corners;
	}
	
	
	public static Rectangle2D bound(Rectangle2D rect, Image... img) {
		if ( null == rect ) {
			rect = new Rectangle2D.Double();
		}
		
		Rectangle2D a = new Rectangle2D.Double();
		
		for (Image i : img) {
			a.setRect(0, 0, i.getWidth(null), i.getHeight(null));
			rect.add(a);
		}

		return rect;
	}
	
	public static Rectangle2D bound(Rectangle2D rect, Rectangle2D... rects) {
		if ( null == rect ) {
			rect = new Rectangle2D.Double();
		}
		
		for (Rectangle2D r : rects) {
			rect.add(r);
		}

		return rect;
	}

	public static Rectangle2D bound(Rectangle2D rect, Point2D... points) {
		if ( null == rect ) {
			rect = new Rectangle2D.Double();
		}
		
		for (Point2D p : points) {
			rect.add(p);
		}

		return rect;
	}

}
