package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.ImageProcessorBase;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsImageSerializer;

public abstract class IPFModJavaAffineTransformation extends ImageProcessorBase implements IPFModJavaComponents {

	AffineTransform at;

	protected BufferedImage apply(BufferedImage img) {

		Rectangle2D rect = IPFModJavaUtils.bound(null, img);
		Point2D[] corners = IPFModJavaUtils.getCorners(rect, null);

		at.transform(corners, 0, corners, 0, corners.length);

		rect = IPFModJavaUtils.bound(null, corners);

		BufferedImage ret = new BufferedImage((int) rect.getWidth(), (int) rect.getHeight(), img.getType());
		
		double shx = -rect.getMinX();
		double shy = -rect.getMinY();
		
		AffineTransform at1 = AffineTransform.getTranslateInstance(shx, shy);
		at1.concatenate(at);
		
		at = at1;

		Graphics2D dispGc = ret.createGraphics();
		dispGc.drawImage(img, at, null);
		
		return ret;
	}
	
	protected void apply(ProcessContext context, String inImage, String outImage) throws Exception {
		IPFUtilsImageSerializer is = new IPFUtilsImageSerializer();

		context.readStream(inImage, is, null);
		
		BufferedImage img = is.getImg();

		img = apply(img);
		
		is.setImg(img);
		
		context.writeStream(outImage, is);
	}
	
	protected void apply(ProcessContext context) throws Exception {
		apply(context, GenericFields.Image.name(), GenericFields.Image.name());
	}	

	public static class Rotate extends IPFModJavaAffineTransformation {

		@Override
		public ProcessResult execute(ProcessContext context) throws Exception {
			DataObject self = getSelf();
			Double angle = IPFUtilsCommon.getAttribute(self, ProcessFields.angle);
			if ( null == angle ) {
				angle = 0.0;
			}
			at = AffineTransform.getRotateInstance(angle);
			
			apply(context);

			return ProcessResult.finished;
		}
	}

}
