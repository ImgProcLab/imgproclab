package hu.uni_pannon.mik.mat.ipf.modules.javabase;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.ImageProcessorBase;

public class IPFModJavaSystemCommand extends ImageProcessorBase implements IPFModJavaComponents {
	private static final String PSTART = "${";
	private static final String PEND = "}";
	
	@Override
	public ProcessResult execute(ProcessContext context) throws Exception {
		DataObject self = getSelf();
		
		StringBuilder commandPattern = new StringBuilder( self.getAttribute("commandPattern"));
		int i, e=0;
		
		while ( -1 != (i = commandPattern.indexOf(PSTART))) {
			e = commandPattern.indexOf(PEND, i);
			String key = commandPattern.substring(i+PSTART.length(), e);
			
			String pVal = self.getAttribute(key);
			commandPattern = commandPattern.replace(i, e+PEND.length(), pVal);
		}
		
		String command = commandPattern.toString();
		
		command = command.replace("/", "\\");
		
		System.out.println("Executing system command: " + command);
		
		Process p = Runtime.getRuntime().exec(command);
		p.waitFor();
		
		System.out.println("Done.");
		
		return ProcessResult.processed;
	}

}
