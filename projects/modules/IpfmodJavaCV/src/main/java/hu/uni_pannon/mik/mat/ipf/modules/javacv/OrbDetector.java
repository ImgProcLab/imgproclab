package hu.uni_pannon.mik.mat.ipf.modules.javacv;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.swing.JFrame;

import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_features2d;
import org.bytedeco.javacpp.opencv_features2d.ORB;
import org.bytedeco.javacpp.opencv_imgcodecs;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.ImageProcessorBase;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public class OrbDetector extends ImageProcessorBase implements IPFSharedComponents {

	@Override
	public ProcessResult execute(ProcessContext context) throws Exception {
		DataObject self = getSelf();

		Long features = IPFUtilsCommon.getAttribute(self, "features");
		Double scaleFactor = IPFUtilsCommon.getAttribute(self, "scaleFactor");
		Long levels = IPFUtilsCommon.getAttribute(self, "levels");
		Long edgeThreshold = IPFUtilsCommon.getAttribute(self, "edgeThreshold");
		Long firstLevel = IPFUtilsCommon.getAttribute(self, "firstLevel");
		Long WTA_K = IPFUtilsCommon.getAttribute(self, "WTA_K");
		Long scoreType = IPFUtilsCommon.getAttribute(self, "scoreType");
		Long patchSize = IPFUtilsCommon.getAttribute(self, "patchSize");
		Long fastThreshold = IPFUtilsCommon.getAttribute(self, "fastThreshold");

		String fileName = IPFUtilsCommon.getAttribute(self, "fileName");
		String cmpId = IPFUtilsCommon.getAttribute(self, "cmpId");

		ORB orb = opencv_features2d.ORB.create(features.intValue(), scaleFactor.floatValue(), levels.intValue(), edgeThreshold.intValue(), firstLevel.intValue(), WTA_K.intValue(),
				scoreType.intValue(), patchSize.intValue(), fastThreshold.intValue());

		String inDir = IPFUtilsCommon.getAttribute(self, "inDir");
		String inMatch = IPFUtilsCommon.getAttribute(self, "inMatch");
		String outDir = IPFUtilsCommon.getAttribute(self, "outDir");

		if (!IPFUtilsBasic.strIsEmpty(inDir) && !IPFUtilsBasic.strIsEmpty(outDir)) {
			File fSource = new File( inDir );
			Pattern p = IPFUtilsBasic.strIsEmpty(inDir) ? null : Pattern.compile(inMatch);
			
			File fTarget = new File( outDir );
			if ( fTarget.exists() ) {
				for (File file: fTarget.listFiles()) {
					if (!file.isDirectory()) {
						file.delete();
					}
				}
			} else {
				fTarget.mkdirs();
			}
			
			for (File f : fSource.listFiles()) {
				String fn = f.getName();
				if (f.isFile() && ((null == p ) || p.matcher(fn).matches())) {
					process(orb, f.getAbsolutePath(), new File(outDir, fn).getAbsolutePath());
				}
			}
		} 
			
		if (!IPFUtilsBasic.strIsEmpty(fileName)) {
			demo(orb, fileName, cmpId);
		}

		// Mat s1 = opencv_imgcodecs.imread(defaultImage);
		//
		// KeyPointVector keypoints = new KeyPointVector();
		// orb.detect(s1, keypoints);
		//
		// opencv_features2d.drawKeypoints(s1, keypoints, s1);
		//
		// Frame frame = new OpenCVFrameConverter.ToMat().convert(s1);
		//
		// BufferedImage result = converter.getBufferedImage(frame, 1.0, false,
		// null);
		//
		// IPFUtilsImageSerializer is = new IPFUtilsImageSerializer();
		//
		// String inImage = GenericFields.Image.name();
		// String outImage = GenericFields.Image.name();
		//
		// context.readStream(inImage, is, null);
		// BufferedImage img = is.getImg();
		//
		// ToIplImage iplConverter = new OpenCVFrameConverter.ToIplImage();
		// Java2DFrameConverter java2dConverter = new Java2DFrameConverter();
		// IplImage iplImage = iplConverter.convert(java2dConverter.convert(img));
		//
		// Mat imgMat = new Mat(iplImage); // true - required to copy data
		//
		// is.setImg(result);
		//
		// context.writeStream(outImage, is);

		return ProcessResult.processed;
	}

	public void process(ORB orb, String inFile, String outFile) throws Exception {
		Mat s1 = opencv_imgcodecs.imread(inFile);

		KeyPointVector keypoints = new KeyPointVector();
		orb.detect(s1, keypoints);

		Mat dst = new Mat();
		opencv_features2d.drawKeypoints(s1, keypoints, dst);

		opencv_imgcodecs.imwrite(outFile, dst);
	}

	public void demo(ORB orb, String fileName, String cmpId) throws Exception {
		Mat imgMat = opencv_imgcodecs.imread(IPFUtilsBasic.strIsEmpty(fileName) ? defaultImage : fileName);

		KeyPointVector keypoints = new KeyPointVector();
		orb.detect(imgMat, keypoints);

		Mat dst = new Mat();
		opencv_features2d.drawKeypoints(imgMat, keypoints, dst);
		
		StringBuilder cap = new StringBuilder(fileName);
		
		if ( null != cmpId ) {
			cap.append(" - ").append(cmpId);
		}

		show(dst, cap.toString());
	}

	public void show(Mat image, String caption) {
		Frame frame = new OpenCVFrameConverter.ToMat().convert(image);
		show(frame, caption);
	}

	private void show(Frame frame, String caption) {
		CanvasFrame canvas = createCanvas(caption, frame.imageWidth, frame.imageHeight);
		canvas.showImage(frame);
	}

	Map<String, CanvasFrame> displays = new HashMap<>();

	private CanvasFrame createCanvas(String caption, int width, int height) {
		CanvasFrame cf = displays.get(caption);
		
		if (null == cf) {
			cf = new CanvasFrame(caption, 1);
			cf.setCanvasSize(width, height);
			cf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
			if ( displays.isEmpty() ) {
				Runtime.getRuntime().addShutdownHook(new Thread() {
					@Override
					public void run() {
						for ( CanvasFrame cf : displays.values() ) {
							cf.setVisible(false);
						}
					}
				});
			}
			displays.put(caption, cf);
		}
		return cf;
	}

}
