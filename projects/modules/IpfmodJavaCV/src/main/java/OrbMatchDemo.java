import org.bytedeco.javacpp.opencv_core.DMatchVector;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_features2d.ORB;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.bytedeco.javacpp.opencv_features2d;
import org.bytedeco.javacpp.opencv_imgcodecs;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter.ToIplImage;

public class OrbMatchDemo {
	
	static 		opencv_features2d.ORB orb = opencv_features2d.ORB.create(10, 1.2f, 8, 31, 0, 2,
			opencv_features2d.ORB.HARRIS_SCORE, 31, 20);

	
	public static void process(String file) throws Exception {
		Mat s1 = opencv_imgcodecs.imread(file);

		KeyPointVector keypoints = new KeyPointVector();
		orb.detect(s1, keypoints);
		
		Mat dst = new Mat();
		opencv_features2d.drawKeypoints(s1, keypoints, dst);
		
		String f1 = file.replace("\\aa\\", "\\bb\\");
		
		opencv_imgcodecs.imwrite(f1, dst);
	}


	public static void main2(String[] args) throws Exception {
		File fSource = new File( "D:/pemik/lab-git/test/aa");
		for (File f : fSource.listFiles()) {
			if (f.isFile() && f.getName().startsWith("ii")) {
				process(f.getAbsolutePath());
			}
		}
	}

	public static void main(String[] args) throws Exception {
		BufferedImage img = ImageIO.read(new File("Lenna.png"));

		ToIplImage iplConverter = new OpenCVFrameConverter.ToIplImage();
		Java2DFrameConverter java2dConverter = new Java2DFrameConverter();
		IplImage iplImage = iplConverter.convert(java2dConverter.convert(img));

		Mat imgMat = new Mat(iplImage); // true - required to copy data

		ORB orb = opencv_features2d.ORB.create(100, 1.2f, 8, 31, 0, 2, opencv_features2d.ORB.HARRIS_SCORE, 31, 20);
		
		imgMat = opencv_imgcodecs.imread("Lenna.png");

		KeyPointVector keypoints = new KeyPointVector();
		orb.detect(imgMat, keypoints);

		Mat dst = new Mat();
		opencv_features2d.drawKeypoints(imgMat, keypoints, dst);

		show(dst, "Matches");

	}


	public static void old(String[] args) throws Exception {
		String f1 = "D:/pemik/lab-git/test/aa/ii-1.png";
		String f2 = "D:/pemik/lab-git/test/aa/ii-50.png";

		Mat s1 = opencv_imgcodecs.imread(f1);
		show(s1, f1);

		Mat s2 = opencv_imgcodecs.imread(f2);
		show(s2, f2);

		opencv_features2d.ORB orb = opencv_features2d.ORB.create(10, 1.2f, 8, 31, 0, 2,
				opencv_features2d.ORB.HARRIS_SCORE, 31, 20);

		KeyPointVector sourceKeypoints = new KeyPointVector();
		orb.detect(s1, sourceKeypoints);
		showKeypoints(s1, sourceKeypoints, "Features");

		Mat sourceDescriptors = new Mat();
		orb.compute(s1, sourceKeypoints, sourceDescriptors);

		KeyPointVector transformedKeypoints = new KeyPointVector();
		orb.detect(s2, transformedKeypoints);
		showKeypoints(s2, transformedKeypoints, "Transformed features");

		Mat transformedDescriptors = new Mat();
		orb.compute(s2, transformedKeypoints, transformedDescriptors);

		DMatchVector matches = new DMatchVector();
		opencv_features2d.BFMatcher matcher = new opencv_features2d.BFMatcher();
		matcher.match(transformedDescriptors, sourceDescriptors, matches);

		Mat dst = new Mat();
		opencv_features2d.drawMatches(s1, sourceKeypoints, s2, transformedKeypoints, matches, dst);
		show(dst, "Matches");
		
		matcher.close();
	}

	public static void showKeypoints(Mat image, KeyPointVector keypoints, String caption) throws Exception {
		Mat dst = new Mat();
		opencv_features2d.drawKeypoints(image, keypoints, dst);
		show(dst, caption);
	}

	public static void show(Mat image, String caption) {
		Frame frame = new OpenCVFrameConverter.ToMat().convert(image);
		show(frame, caption);
	}

	private static void show(Frame frame, String caption) {
		CanvasFrame canvas = createCanvas(caption, frame.imageWidth, frame.imageHeight);
		canvas.showImage(frame);
	}

	private static CanvasFrame createCanvas(String caption, int width, int height) {
		CanvasFrame canvas = new CanvasFrame(caption, 1);
		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		canvas.setCanvasSize(width, height);
		return canvas;
	}

}
