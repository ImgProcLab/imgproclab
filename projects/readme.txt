This is the root folder of Image Processing Framework (IPF) implementation.

Content:
 - bin: compiled binaries, runnable components of the system;
 - ext: external projects and libraries required to either the framework or the processing modules
 - framework: folder to contain framework projects (core, gui, etc.)
 - IpfShared: Java project with shared declarations and utilities (framework and modules use it)
 - modules: implementation of image processing algorithms
 - test: projects for testing
