package hu.uni_pannon.mik.mat.ipf.swing;

import javax.swing.JInternalFrame;

public class IPFSwingFrameInternal extends JInternalFrame {
	private static final long serialVersionUID = 1L;

	IPFSwingToolsContentPanel content;

	public IPFSwingFrameInternal(IPFSwingToolsContentPanel pnl, boolean palette) throws Exception {
		super("test", true, true, true, false);
		setBounds(10, 10, 600, 400);

		if (!palette) {
			setVisible(true);
		}
		
		setContent(pnl);

		setDefaultCloseOperation(palette ? HIDE_ON_CLOSE : DISPOSE_ON_CLOSE);
	}

	void setContent(IPFSwingToolsContentPanel pnl) throws Exception {
		content = pnl;
		content.init();
		setContentPane(pnl);
		setTitle(content.getContentName());
	}
}