package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JLabel;

import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;

public class IPFSwingToolsHeadLabel {
	String constraint;
	JLabel label;
	Container owner;

	public IPFSwingToolsHeadLabel(Container owner) {
		this(owner, BorderLayout.NORTH);
	}
	
	public IPFSwingToolsHeadLabel(Container owner, String constraint) {
		this.owner = owner;
		this.constraint = constraint;
	}

	public void setHeadText(String text) {
		if ( IPFUtilsBasic.strIsEmpty(text) ) {
			if (null != label ) {
				owner.remove(label);
			}
		} else {
			if ( null == label ) {
				label = new JLabel(text);
			} else {
				label.setText(text);
			}
			
			if ( owner != label.getParent() ) {
				owner.add(label, constraint);
			}
		}
	}
}
