package hu.uni_pannon.mik.mat.ipf.swing;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents;
import hu.uni_pannon.mik.mat.ipf.swing.IPFSwingToolsTreeModel.DataObjectCellRenderer;
import hu.uni_pannon.mik.mat.ipf.swing.IPFSwingToolsTreeModel.DataObjectModel;

public class IPFSwingToolsDataTreePanel extends IPFSwingToolsContentPanel
		implements IPFSharedDataComponents.DataObjectController {
	private static final long serialVersionUID = 1L;

	JTree tree;
	DataObjectModel obModel = new DataObjectModel(this);
	TreeCellRenderer tcr = new DataObjectCellRenderer(this);

	public IPFSwingToolsDataTreePanel(GuiElements type) {
		super(type);
	}

	@Override
	protected JComponent getContent() throws Exception {
		tree = new JTree(obModel);
		tree.setCellRenderer(tcr);

		IPFSwingToolsSelectionControl.includeTree(tree);

		return tree;
	}

	public void setRoot(DataObject root) {
		obModel.setRoot(root);
	}

	public boolean browseChildren(DataObject node) {
		return false;
	}

	public String nodeToString(DataObject node) {
		return node.toString();
	}

}
