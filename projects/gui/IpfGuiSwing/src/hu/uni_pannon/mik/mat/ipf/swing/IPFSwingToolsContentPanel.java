package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;

public class IPFSwingToolsContentPanel extends JPanel implements IPFSwingComponents {
	private static final long serialVersionUID = 1L;

	public final GuiElements type;
	JScrollPane scp;
	boolean toInit = true;
	IPFSwingToolsHeadLabel head;
	
	public IPFSwingToolsContentPanel() {
		this(GuiElements.PlaceholderToBeImplemented);
	}

	public IPFSwingToolsContentPanel(GuiElements type) {
		super(new BorderLayout());
		this.type = type;
		if (type.scrollable) {
			scp = new JScrollPane();
			add(scp, BorderLayout.CENTER);
		}
		head = new IPFSwingToolsHeadLabel(this);
	}

	public void init() throws Exception {
		if (toInit) {
			JComponent content = getContent();

			if (null == scp) {
				add(content, BorderLayout.CENTER);
			} else {
				scp.setViewportView(content);
			}
			
			toInit = false;
		}
	}
	
	void load(Object data) {
		
	}
	
	public void setHead(String head) {
		this.head.setHeadText(head);
	}

	protected JComponent getContent() throws Exception {
		return new JLabel(getContentName());
	}

	public String getContentName() {
		return IPFUtilsBasic.safeToStr(type);
	}

}