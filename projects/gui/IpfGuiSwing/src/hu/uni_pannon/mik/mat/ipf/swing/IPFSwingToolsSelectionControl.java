package hu.uni_pannon.mik.mat.ipf.swing;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;

public class IPFSwingToolsSelectionControl {
	private static Object selectedObject;
	private static Object selectedContainer;
	
	@SuppressWarnings("unchecked")
	public static <T> T getSelectedObject() {
		return (T) selectedObject;
	}
	
	public static Object getSelectedContainer() {
		return selectedContainer;
	}
	
	enum SourceType {
		tree
	}
	
	private static void setSelectedObject(SourceType st, Object ob, Object container) {
		selectedObject = ob;
		selectedContainer = container;
		boolean dragEnabled;
		
		switch ( st ) {
		case tree:
			dragEnabled = !IPFUtilsBasic.strIsEmpty(((DataObject)ob).getAttribute("palettePath"));
			((JTree)container).setDragEnabled(dragEnabled);
			break;
		default:
			break;
		
		}
	}
	
	static TreeSelectionListener tsl = new TreeSelectionListener() {
		@Override
		public void valueChanged(TreeSelectionEvent e) {
			setSelectedObject(SourceType.tree, e.getNewLeadSelectionPath().getLastPathComponent(), e.getSource());
		}
	};
	
	public static void includeTree(JTree tree) {
		tree.addTreeSelectionListener(tsl);
	}
}
