package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JTree;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsPool;

public interface IPFSwingToolsTreeModel extends IPFSwingComponents {
	public static class DataObjectModel implements TreeModel, Resetable {
		private DataObject root;

		Set<TreeModelListener> modelListeners;
		DataObjectController controller;

		public DataObjectModel(DataObjectController controller) {
			this.controller = controller;
		}

		IPFUtilsPool<DataObject, ArrayList<DataObject>> pool = new IPFUtilsPool<DataObject, ArrayList<DataObject>>() {
			@Override
			protected ArrayList<DataObject> create(DataObject key) throws Exception {
				ArrayList<DataObject> ret = new ArrayList<>();

				if (controller.browseChildren(key)) {

					for (String id : key.getAttributeNames()) {
						Object o = key.getAttribute(id);
						if (o instanceof DataObject) {
							ret.add((DataObject) o);
						}
					}
				}
				return ret;
			}
		};

		@Override
		public DataObject getRoot() {
			return root;
		}
		
		public void setRoot(DataObject root) {
			this.root = root;
			reset();
		}

		@Override
		public DataObject getChild(Object parent, int index) {
			return pool.get((DataObject) parent).get(index);
		}

		@Override
		public int getChildCount(Object parent) {
			return pool.get((DataObject) parent).size();
		}

		@Override
		public boolean isLeaf(Object node) {
			return pool.get((DataObject) node).isEmpty();
		}

		@Override
		public void valueForPathChanged(TreePath path, Object newValue) {
			// TODO Auto-generated method stub
		}

		@Override
		public int getIndexOfChild(Object parent, Object child) {
			return pool.get((DataObject) parent).indexOf(child);
		}

		@Override
		public void addTreeModelListener(TreeModelListener l) {
			if (null == modelListeners) {
				modelListeners = new HashSet<>();
			}
			modelListeners.add(l);
		}

		@Override
		public void removeTreeModelListener(TreeModelListener l) {
			if (null != modelListeners) {
				modelListeners.remove(l);
			}
		}

		@Override
		public void reset() {
			pool.reset();
			if (null != modelListeners) {
				TreeModelEvent e = new TreeModelEvent(this, new DataObject[] { root });
				for (TreeModelListener l : modelListeners) {
					l.treeStructureChanged(e);
				}
			}
		}
	}
	
	public static class DataObjectCellRenderer extends DefaultTreeCellRenderer {
		private static final long serialVersionUID = 1L;
		
		DataObjectFormatter formatter;
		
		public DataObjectCellRenderer(DataObjectFormatter formatter) {
			this.formatter = formatter;
		}

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
				boolean leaf, int row, boolean hasFocus) {
			DataObject item = (DataObject) value;

			String label = formatter.nodeToString(item);
			return super.getTreeCellRendererComponent(tree, label, sel, expanded, leaf, row, hasFocus);
		}
	};

}
