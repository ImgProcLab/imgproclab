package hu.uni_pannon.mik.mat.ipf.swing;

import javax.swing.JComponent;

public class IPFSwingPanelDataImage extends IPFSwingToolsContentPanel {
	private static final long serialVersionUID = 1L;

	IPFSwingToolsImagePanel ip;

	public IPFSwingPanelDataImage() {
		super(GuiElements.PanelImage);
		ip = new IPFSwingToolsImagePanel();
	}

	public IPFSwingPanelDataImage(String fileName) {
		this();
		setImage(fileName);
	}

	@Override
	protected JComponent getContent() {
		return ip;
	}

	public void setImage(String fileName) {
		ip.setImage(fileName);
	}

}
