package hu.uni_pannon.mik.mat.ipf.swing;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsPool;

class IPFSwingToolsListSwitchPanel extends IPFSwingToolsContentPanel {
	private static final long serialVersionUID = 1L;

	JList<Object> lItems;
	ListModel<Object> mdlList;
	JSplitPane splListContent;

	IPFUtilsPool<GuiElements, IPFSwingToolsContentPanel> contentPool = new IPFUtilsPool<GuiElements, IPFSwingToolsContentPanel>() {

		@Override
		protected IPFSwingToolsContentPanel create(GuiElements key) throws Exception {
			return new IPFSwingToolsContentPanel(key);
		}
	};

	public IPFSwingToolsListSwitchPanel() {
		super(GuiElements.PanelDataList);
		
		DefaultListModel<Object> lm = new DefaultListModel<Object>();

		mdlList = lm;
		lItems = new JList<>(mdlList);

		splListContent = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

		JScrollPane scList = new JScrollPane();
		scList.setViewportView(lItems);

		splListContent.setLeftComponent(scList);

		lItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lItems.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					selectItem();
				}
			}
		});
		
	}
	
	@Override
	protected JComponent getContent() {
		return splListContent;
	}

	void selectItem() {
		Object o = lItems.getSelectedValue();
		GuiElements nType = GuiElements.PanelImage; // to be replaced with type
																								// selection from the object

		IPFSwingToolsContentPanel tp = (IPFSwingToolsContentPanel) splListContent.getRightComponent();
		
		if ((null == tp) || (nType != tp.type)) {
			tp = contentPool.get(nType);
			splListContent.setRightComponent(tp);
		}
		
		tp.load(o);
	}
}