package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

public class IPFSwingToolsSizingDesktop extends JDesktopPane {
	private static final long serialVersionUID = 1L;
	
	Dimension d = new Dimension();

	public void paint(Graphics g) {
		super.paint(g);

		updateSize();

		this.revalidate();
	}

	public void updateSize() {
		int maxX = 0;
		int maxY = 0;
		for (JInternalFrame jif : getAllFrames()) {
			if (jif.isVisible()) {
				int i = jif.getX() + jif.getWidth();
				if (i > maxX) {
					maxX = i;
				}
				i = jif.getY() + jif.getHeight();
				if (i > maxY)
					maxY = i;
			}
		}

		d.height = maxY;
		d.width = maxX;

		setPreferredSize(d);
	}
}