package hu.uni_pannon.mik.mat.ipf.swing;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class IPFSwingToolsTableModelBase <T extends Enum<T>> extends AbstractTableModel implements IPFSwingComponents {
	T[] cols;
	
	public IPFSwingToolsTableModelBase(Class<T> enumClass) {
		cols = enumClass.getEnumConstants();
	}

	@Override
	public int getColumnCount() {
		return cols.length;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return (cols[columnIndex] instanceof EditController) ? ((EditController)cols[columnIndex]).isEditable() : true;
	}
	
	@Override
	public String getColumnName(int column) {
		return cols[column].name();
	}
	
	@Override
	public int getRowCount() {
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return cols[columnIndex].name() + " " + rowIndex;
	}
}