package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsPool;
import hu.uni_pannon.mik.mat.ipf.swing.IPFSwingPanelChain.Commands;

public interface IPFSwingComponents extends IPFSharedComponents, IPFSharedDataComponents {
	public enum MenuHeads {
		Palettes, Algorithms, Processes, Data
	}

	public enum GuiElements {
		PanelAlgorithm(MenuHeads.Algorithms, false), PanelDataList(MenuHeads.Data, false), PanelImage(MenuHeads.Data), PanelData(MenuHeads.Data), PaletteAlgorithm(MenuHeads.Palettes), PaletteWorkspace(
				MenuHeads.Palettes), PaletteDefaultData(MenuHeads.Palettes, false), PlaceholderToBeImplemented(null, false), PanelProcess(MenuHeads.Processes, false);

		public final boolean palette;
		public final MenuHeads menuHead;
		public final boolean scrollable;

		private GuiElements(MenuHeads menuHead) {
			this(menuHead, true);
		}

		private GuiElements(MenuHeads menuHead, boolean scrollable) {
			this.palette = MenuHeads.Palettes == menuHead;
			this.menuHead = menuHead;
			this.scrollable = scrollable;
		}
	}

	enum FrameCommands {
	}
	
	interface CtrlProvider {
		Class<? extends JComponent> getCtrlClass();
	}
	
	public static abstract class CommandListener<T extends Enum<T>> implements ActionListener {
		Class<T> cmdEnumClass;
		
		IPFUtilsPool<T, JComponent> cmdCtrls = new IPFUtilsPool<T, JComponent>(){
			@Override
			protected JComponent create(T key) throws Exception {
				Class<? extends JComponent> ctrlClass = (key instanceof CtrlProvider) ? ((CtrlProvider)key).getCtrlClass() : JButton.class; 
				if ( null == ctrlClass ) {
					ctrlClass = JButton.class;
				}
				JComponent ret = ctrlClass.newInstance();
				
				if (ret instanceof AbstractButton) {
					AbstractButton ab = (AbstractButton) ret;
					ab.setText(key.name());
					ab.setActionCommand(key.name());
					ab.addActionListener(CommandListener.this);
				}
				return ret;
			}};
		
		public CommandListener(Class<T> enumClass) {
			this.cmdEnumClass = enumClass;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			T cmd = IPFUtilsBasic.enumForOb(e.getActionCommand(), cmdEnumClass, null);
			try {
				doCommand(cmd, e);
			} catch (Throwable t) {
				IPFUtilsCommon.safeSwallowException(t, GenericEvents.cmdExecutionException, false);
			}
		}
		
		public JPanel getPanel(boolean horizontal) {
			GridLayout l = horizontal ? new GridLayout(1, Commands.values().length) : new GridLayout(Commands.values().length, 1); 
			JPanel btns = new JPanel(l);
			for (T c : cmdEnumClass.getEnumConstants() ) {
				btns.add(getButton(c));
			}

			return btns;
		}
		
		public JComponent getButton(T cmd) {
			return cmdCtrls.get(cmd);
		}
		
		abstract void doCommand(T cmd, ActionEvent e) throws Exception;
	};

}
