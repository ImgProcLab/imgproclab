package hu.uni_pannon.mik.mat.ipf.swing;

import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;

public class IPFSwingPanelDataEdit extends IPFSwingToolsContentPanel {
	private static final long serialVersionUID = 1L;

	DataObject type;
	DataObject object;

	class AttInfo {
		DataObject def;
		public final String id;
		public final ValueType at;

		public AttInfo(String id, DataObject def) {
			this.def = def;
			this.id = id;
			
			at = IPFUtilsBasic.enumForOb(def.getAttribute("valueType"), ValueType.class, ValueType.attString);
		}
	};

	ArrayList<AttInfo> attributes = new ArrayList<>();

	enum AttCols {
		name, value
	}

	class NodeTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		@Override
		public int getRowCount() {
			return attributes.size();
		}

		@Override
		public int getColumnCount() {
			return AttCols.values().length;
		}

		public String getColumnName(int column) {
			return AttCols.values()[column].name();
		};

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return editable && (1 == columnIndex);
		};

		public java.lang.Class<?> getColumnClass(int columnIndex) {
			return String.class;
		};

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			AttInfo ai = attributes.get(rowIndex);
			switch (columnIndex) {
			case 0:
				return ai.id;
			case 1:
				return object.getAttribute(ai.id);
			}
			return null;
		}

		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			AttInfo ai = attributes.get(rowIndex);
			object.setAttribute(ai.id, ai.at.stringToValue((String)aValue));
		};
		
		void update() {
			fireTableDataChanged();
		}

	};

	NodeTableModel tm;
	JTable tbl;
	boolean editable;

	public IPFSwingPanelDataEdit() {
		super(GuiElements.PanelData);
		tm = new NodeTableModel();
		tbl = new JTable(tm);
	}

	public void init(DataObject type, DataObject object, boolean editable) throws Exception {
		super.init();
		
		this.type = type;
		this.object = object;

		attributes.clear();
		for (String n : type.getAttributeNames()) {
			attributes.add(new AttInfo(n, type.getAttribute(n)));
		}
		
		tm.update();
		
		setEditable(editable);
	}

	@Override
	protected JComponent getContent() throws Exception {
		return tbl;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}
}
