package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.TransferHandler;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataChangeListener;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.MessageFields;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsContext;

public class IPFSwingPanelChain extends IPFSwingToolsContentPanel implements DataChangeListener {
	private static final long serialVersionUID = 1L;

	public enum Commands {
		Run, Step, Reset;
	}

	ProcessContext pctx;
	DataObject process;
	
	int current;

	DefaultListModel<DataObject> chainModel = new DefaultListModel<DataObject>();
	JList<DataObject> chainList = new JList<DataObject>(chainModel);
	
	CommandListener<Commands> cmdListener = new CommandListener<Commands>(Commands.class) {
		@Override
		void doCommand(Commands cmd, ActionEvent e) throws Exception {
			switch (cmd) {
			case Run:
				DataObject msg = IPFUtilsCommon.createDataObject("msg");

				msg.setAttribute(MessageFields.Context, pctx);

				IPFUtilsCommon.send(process, msg);

				break;

			default:
				System.out.println("Command received: " + cmd);
				break;
			}
		}
	};
	IPFSwingToolsSaverPanel saverPanel = new IPFSwingToolsSaverPanel();

	
	class ReorderListener extends MouseAdapter {
	   private int pressIndex = 0;
	   private int releaseIndex = 0;

	   @Override
	   public void mousePressed(MouseEvent e) {
	      pressIndex = chainList.locationToIndex(e.getPoint());
	   }

	   @Override
	   public void mouseReleased(MouseEvent e) {
	      releaseIndex = chainList.locationToIndex(e.getPoint());
	      if (releaseIndex != pressIndex && releaseIndex != -1) {
	         reorder();
	      }
	   }

	   @Override
	   public void mouseDragged(MouseEvent e) {
	      mouseReleased(e);
	      pressIndex = releaseIndex;      
	   }

	   private void reorder() {
	      DataObject dragee = chainModel.elementAt(pressIndex);
	      chainModel.removeElementAt(pressIndex);
	      chainModel.insertElementAt(dragee, releaseIndex);
	      
				updateProcess();
	   }
	   
	   public void mouseClicked(MouseEvent event)
	   {
	     if (event.getClickCount() == 2) {
	       DataObject alg = chainModel.getElementAt(pressIndex);
	       
	       IPFSwingFrameHost.theFrame.showAlgorithm(alg);
	     }
	   }
	}
	

  public class ListTransferHandler extends TransferHandler {

      public boolean canImport(TransferHandler.TransferSupport info) {
          return true;
     }
      /**
       * We support only move actions.
       */
      public int getSourceActions(JComponent c) {
          return TransferHandler.NONE;
      }
      /**
       * Perform the actual import.  This demo only supports drag and drop.
       */
      public boolean importData(TransferHandler.TransferSupport info) {
          if (!info.isDrop()) {
              return false;
          }
          
          JList.DropLocation dl = (JList.DropLocation)info.getDropLocation();
          int index = dl.getIndex();

					DataObject ob = IPFSwingToolsSelectionControl.getSelectedObject();
					
					insertAlgorithm(ob, index);

          
          return true;
      }
  }

	public IPFSwingPanelChain() {
		super(GuiElements.PanelProcess);
		
		DataObject algType = IPFUtilsCommon.getRootObject("IPFModuleJava.Chain");
		process = IPFUtilsCommon.createObjectByType(algType);
		process.setListener(ListenerOp.add, this);

		pctx = new IPFUtilsContext();
				
		chainList.addMouseListener(new ReorderListener());
		
		chainList.setDropMode(DropMode.INSERT);
		chainList.setTransferHandler(new ListTransferHandler());
	}
	
	void addAlgorithm(String typeName) {
		DataObject algType = IPFUtilsCommon.getRootObject(typeName);
		insertAlgorithm(algType, -1);
	}
	
	void insertAlgorithm(DataObject algType, int idx) {
		DataObject algorithm = IPFUtilsCommon.createObjectByType(algType);
		if ( (-1 == idx ) || (idx >= chainModel.size())) {
			chainModel.addElement(algorithm);
		} else {
			chainModel.insertElementAt(algorithm, idx);
		}
		
		updateProcess();
	}

	@Override
	protected JComponent getContent() throws Exception {
		JPanel ret = new JPanel(new BorderLayout());

		JPanel btns = new JPanel(new GridLayout(1, Commands.values().length));
		for (Commands c : Commands.values()) {
			btns.add(cmdListener.getButton(c));
		}
		ret.add(btns, BorderLayout.SOUTH);
		
		ret.add(new JScrollPane(chainList), BorderLayout.CENTER);

		ret.add(saverPanel, BorderLayout.NORTH);
		
		saverPanel.setObject(process);

		return ret;
	}
	
	void updateProcess() {
		process.removeAttribute("Members");
		
		int count = chainModel.size();
		for ( int i = 0; i < count; ++i ) {
			process.setAttribute("Members", i, chainModel.getElementAt(i));
		}
	}

	@Override
	public void dataChanged(DataChangeEvent event) {
		// TODO Auto-generated method stub

	}
}
