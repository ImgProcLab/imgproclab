package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.EnumMap;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.table.TableModel;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataChangeListener;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsContext;

public class IPFSwingPanelProcess extends IPFSwingToolsContentPanel implements DataChangeListener {
	private static final long serialVersionUID = 1L;

	public enum Area {
		Chain, Mapping, ContextData, Run, TopLeft, Left, Main;
	}

	public enum Commands {
		Run, Step, Save, Clear;
	}

	public enum ColChain implements EditController {
		Status, Type, Name, Break;

		@Override
		public boolean isEditable() {
			return Name == this;
		}
	}

	public enum ColMapping implements EditController {
		Direction, Algorithm, Context;
		
		@Override
		public boolean isEditable() {
			return false;
		}
	}

	public enum ColContext implements EditController {
		Name, Type, SetBy;

		@Override
		public boolean isEditable() {
			return Name == this;
		}
	}

	public enum ColMessages {
		Time, Level, Message;
	}

	TableModel tmChain = new IPFSwingToolsTableModelBase<ColChain>(ColChain.class) {
	};

	TableModel tmMapping = new IPFSwingToolsTableModelBase<ColMapping>(ColMapping.class) {
	};

	TableModel tmContext = new IPFSwingToolsTableModelBase<ColContext>(ColContext.class) {
	};

	TableModel tmMessages = new IPFSwingToolsTableModelBase<ColMessages>(ColMessages.class) {
	};

	CommandListener<Commands> cmdListener = new CommandListener<Commands>(Commands.class) {
		@Override
		void doCommand(Commands cmd, ActionEvent e) throws Exception {
			// TODO Auto-generated method stub

		}
	};

	EnumMap<Area, JComponent> areas = new EnumMap<>(Area.class);

	ProcessContext pctx;
	DataObject process;

	public IPFSwingPanelProcess() {
		super(GuiElements.PanelProcess);

		process = IPFUtilsCommon.createDataObject(null);
		process.setListener(ListenerOp.add, this);

		pctx = new IPFUtilsContext();
	}

	JComponent createArea(Area a) {
		JComponent ret = null;
		JSplitPane spl = null;

		switch (a) {
		case TopLeft:
			JTable tblChain = new JTable(tmChain);
			JTable tblMapping = new JTable(tmMapping);

			spl = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(tblChain), new JScrollPane(tblMapping));
			spl.setResizeWeight(0.8);
			break;
		case ContextData:
			JTable tblCtx = new JTable(tmContext);
			spl = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
			spl.setLeftComponent(new JScrollPane(tblCtx));
			spl.setRightComponent(new JScrollPane(new JTextArea("Here comes the switching display panel")));
			spl.setResizeWeight(0.5);
			break;
		case Run:
			ret = new JPanel(new BorderLayout());

			JPanel btns = new JPanel(new GridLayout(Commands.values().length, 1));
			for (Commands c : Commands.values()) {
				btns.add(cmdListener.getButton(c));
			}
			ret.add(btns, BorderLayout.WEST);

			JTable tblMsgs = new JTable(tmMessages);

			JTree trRun = new JTree();

			spl = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
			spl.setLeftComponent(new JScrollPane(trRun));
			spl.setRightComponent(new JScrollPane(tblMsgs));
			spl.setResizeWeight(0.3);

			ret.add(spl, BorderLayout.CENTER);
			break;


		case Left:
			spl = new JSplitPane(JSplitPane.VERTICAL_SPLIT, areas.get(Area.TopLeft), areas.get(Area.Run));
			spl.setResizeWeight(0.8);
			break;

		case Main:
			IPFSwingToolsSaverPanel sp = new IPFSwingToolsSaverPanel();

			spl = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, areas.get(Area.Left), areas.get(Area.ContextData));
			spl.setResizeWeight(0.8);
			
			ret = new JPanel(new BorderLayout());
			ret.add(sp, BorderLayout.SOUTH);
			ret.add(spl, BorderLayout.CENTER);
			break;
		}

		return (null == ret) ? spl : ret;
	}

	@Override
	protected JComponent getContent() throws Exception {
		for (Area a : Area.values()) {
			areas.put(a, createArea(a));
		}

		return areas.get(Area.Main);
	}

	@Override
	public void dataChanged(DataChangeEvent event) {
		if (process == event.getObject()) {
			run();
		}
	}

	protected void run() {
		try {
		} catch (Throwable t) {
			t.printStackTrace(System.err);
		}
	}
}
