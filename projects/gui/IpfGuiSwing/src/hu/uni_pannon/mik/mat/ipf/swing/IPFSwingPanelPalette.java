package hu.uni_pannon.mik.mat.ipf.swing;

import javax.swing.JComponent;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.CmdCall;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.LogicWrapper;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public class IPFSwingPanelPalette extends IPFSwingToolsDataTreePanel {
	private static final long serialVersionUID = 1L;

	public static class Logic extends LogicWrapper<IPFSwingPanelPalette> {

		@Override
		protected IPFSwingPanelPalette createWrapped(DataObject self) throws Exception {
			IPFSwingPanelPalette frm = new IPFSwingPanelPalette();

			return frm;
		}

		@Override
		protected ProcessResult process(CmdCall cmd, DataObject param) throws Exception {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public IPFSwingPanelPalette() {
		super(GuiElements.PaletteAlgorithm);
	}
	
	@Override
	public boolean browseChildren(DataObject node) {
		String palettePath = node.getAttribute("palettePath");
		return IPFUtilsBasic.strIsEmpty(palettePath);
	}
	
	@Override
	public String nodeToString(DataObject node) {
		String label = node.getAttribute("label");

		if (IPFUtilsBasic.strIsEmpty(label)) {
			label = node.getAttribute(SPEC_FLD_ID);
			if (!IPFUtilsBasic.strIsEmpty(label)) {
				int i = label.lastIndexOf('.');
				if (-1 != i) {
					label = label.substring(i + 1);
				}
			} else {
				label = node.toString();
			}
		}
		
		return label;
	}

	@Override
	protected JComponent getContent() throws Exception {
		final DataObject paletteRoot = IPFUtilsCommon.createDataObject(null);
		paletteRoot.setAttribute("label", "Algorithms");

		IPFUtilsCommon.searchRootObjects("generic.type", new DataProcessorDefault<DataObject>() {
			@Override
			public ProcessResult processData(Object key, DataObject data) throws Exception {
				String palettePath = data.getAttribute("palettePath");

				if (IPFUtilsBasic.strIsEmpty(palettePath)) {
					return ProcessResult.skipped;
				} else {
					DataObject o = paletteRoot;
					String[] pi = palettePath.split("/");
					int l = pi.length;
					for ( int i = 0; i < l-1; ++i) {
						DataObject n = o.getAttribute(pi[i]);
						if ( null == n ) {
							n = IPFUtilsCommon.createDataObject(null);
							n.setAttribute("label", pi[i]);
							o.setAttribute(pi[i], n);
						}
						o = n;
					}
					o.setAttribute(pi[l-1], data);
					
					return ProcessResult.processed;
				}
			}
		});

		setRoot(paletteRoot);

		return super.getContent();
	}
}
