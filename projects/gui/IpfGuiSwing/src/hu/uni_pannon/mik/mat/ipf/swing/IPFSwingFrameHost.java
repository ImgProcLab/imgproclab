package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.TooManyListenersException;

import javax.swing.AbstractButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.CmdCall;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.LogicWrapper;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public class IPFSwingFrameHost extends JFrame implements IPFSwingComponents {
	private static final long serialVersionUID = 1L;

	public static class Logic extends LogicWrapper<IPFSwingFrameHost> {

		@Override
		protected IPFSwingFrameHost createWrapped(DataObject self) throws Exception {
			IPFSwingFrameHost frm = new IPFSwingFrameHost();

			frm.setVisible(true);

			return frm;
		}

		@Override
		protected ProcessResult process(CmdCall cmd, DataObject param) throws Exception {
			// TODO Auto-generated method stub
			return null;
		}
	}

	InternalFrameAdapter contentAdapter = new InternalFrameAdapter() {
		public void internalFrameActivated(InternalFrameEvent e) {
			showPalette(((IPFSwingFrameInternal) e.getInternalFrame()).content.type, true);
		};

		@Override
		public void internalFrameDeactivated(InternalFrameEvent e) {
			// showPalette(((IPFSwingFrameInternal)
			// e.getInternalFrame()).content.type, false);
		}

		@Override
		public void internalFrameClosing(InternalFrameEvent e) {
			showPalette(((IPFSwingFrameInternal) e.getInternalFrame()).content.type, false);
		}
	};

	ActionListener paletteMenuListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			GuiElements p = GuiElements.valueOf(e.getActionCommand());
			boolean sel = ((AbstractButton) e.getSource()).isSelected();
			showPalette(p, sel);
		}
	};

	ActionListener dataMenuListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			GuiElements p = GuiElements.valueOf(e.getActionCommand());
			boolean sel = ((AbstractButton) e.getSource()).isSelected();
			showPalette(p, sel);
		}
	};

	static IPFSwingFrameHost theFrame;

	String currentProject;

	JScrollPane scpMain;
	JDesktopPane dpMain;
	JTable tblMessages;

	EnumMap<MenuHeads, JMenu> menus = new EnumMap<>(MenuHeads.class);

	EnumMap<GuiElements, IPFSwingFrameInternal> palettes = new EnumMap<>(GuiElements.class);
	Map<DataObject, IPFSwingFrameInternal> dataFrames = new HashMap<>();

	void showPalette(GuiElements elementType, boolean show) {
		if (elementType.palette) {
			palettes.get(elementType).setVisible(show);

			JMenu m = menus.get(elementType.menuHead);

			for (Component me : m.getMenuComponents()) {
				JCheckBoxMenuItem c = (JCheckBoxMenuItem) me;
				if (elementType.name().equals(c.getActionCommand())) {
					c.setSelected(show);
					break;
				}
			}
		}
	}

	public IPFSwingFrameInternal openContentFrame(IPFSwingToolsContentPanel content) {
		IPFSwingFrameInternal pf = null;

		try {
			pf = new IPFSwingFrameInternal(content, true);
			if (content.type.palette) {
				palettes.put(content.type, pf);
			} else {
				pf.setVisible(true);
			}
			pf.addInternalFrameListener(contentAdapter);
			dpMain.add(pf, JDesktopPane.DEFAULT_LAYER);
			pf.setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.toString(), "Frame creation error!", JOptionPane.ERROR_MESSAGE);
		}

		return pf;
	}

	public void addMenu(JMenu menu, GuiElements cmd) {
		addMenu(menu, cmd, new IPFSwingToolsContentPanel(cmd));
	}

	public void addMenu(JMenu menu, GuiElements cmd, IPFSwingToolsContentPanel pnl) {
		JCheckBoxMenuItem cbi = new JCheckBoxMenuItem(cmd.name());
		cbi.setActionCommand(cmd.name());
		cbi.addActionListener(paletteMenuListener);
		menu.add(cbi);

		openContentFrame(pnl);
	}

	DropTarget dt = new DropTarget();

	public IPFSwingFrameHost() {
		theFrame = this;

		dpMain = new IPFSwingToolsSizingDesktop();
		dpMain.setPreferredSize(new Dimension(800, 600));

		tblMessages = new JTable();

		dt.setActive(true);
		try {
			dt.addDropTargetListener(new DropTargetAdapter() {

				@Override
				public void drop(DropTargetDropEvent dtde) {
					DataObject ob = IPFSwingToolsSelectionControl.getSelectedObject();

					IPFSwingPanelAlgorithm ipp = new IPFSwingPanelAlgorithm(ob, true);
					openContentFrame(ipp);
				}
			});
		} catch (TooManyListenersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dpMain.setDropTarget(dt);

		scpMain = new JScrollPane();
		scpMain.setViewportView(dpMain);

		JMenuBar jmb = new JMenuBar();
		for (MenuHeads mh : MenuHeads.values()) {
			JMenu m = new JMenu(IPFUtilsCommon.getResText(mh));
			jmb.add(m);
			menus.put(mh, m);
		}

		JMenu m = menus.get(MenuHeads.Palettes);

		DataObject ob = IPFUtilsCommon.createDataObject("IPFGui.PaletteAlgorithms");
		IPFSwingToolsContentPanel pnl = (IPFSwingToolsContentPanel) IPFUtilsCommon.getWrappedObject(ob);

		addMenu(m, GuiElements.PaletteAlgorithm, pnl);
		addMenu(m, GuiElements.PaletteWorkspace);
		addMenu(m, GuiElements.PaletteDefaultData);

		m = menus.get(MenuHeads.Processes);
		JMenuItem mi;

		mi = new JMenuItem("New!");
		mi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				IPFSwingToolsContentPanel ipp;
				ipp = new IPFSwingPanelChain();
				openContentFrame(ipp);
			}
		});
		m.add(mi);

		setJMenuBar(jmb);

		// JScrollPane scpMsg = new JScrollPane();
		// scpMsg.setViewportView(tblMessages);
		//
		// JSplitPane spl = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		// spl.setLeftComponent(scpMain);
		// spl.setRightComponent(scpMsg);
		// spl.setResizeWeight(1.0);

		setContentPane(scpMain);

		updateTitle();

		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		showPalette(GuiElements.PaletteAlgorithm, true);
	}

	void showAlgorithm(DataObject algorithm) {
		IPFSwingFrameInternal ipf = dataFrames.get(algorithm);

		if (null == ipf) {
			IPFSwingPanelAlgorithm ipp = new IPFSwingPanelAlgorithm(algorithm, false);
			dataFrames.put(algorithm, openContentFrame(ipp));
		} else {
			try {
				ipf.setSelected(true);
			} catch (PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	void updateTitle() {
		setTitle(IPFUtilsBasic.buildStr(" - ", IPFUtilsCommon.getResText(SharedResIds.apptitle), currentProject));
	}

}
