package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

class IPFSwingToolsImagePanel extends JPanel {
	private static final long serialVersionUID = 1L;

	private BufferedImage image;
	AffineTransform transform;

	public void setImage(String fileName) {
		try {
			image = ImageIO.read(new File(fileName));
			// transform = AffineTransform.getShearInstance(2, 4);
			setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		invalidate();
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (null == image) {
			g.drawString("Failed to load image", 10, 10);
		} else {
			((Graphics2D) g).drawImage(image, transform, null);
		}
	}
}