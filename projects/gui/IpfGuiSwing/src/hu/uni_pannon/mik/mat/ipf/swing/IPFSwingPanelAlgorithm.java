package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.EnumMap;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataChangeListener;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.MessageFields;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsContext;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsImageSerializer;

public class IPFSwingPanelAlgorithm extends IPFSwingToolsContentPanel implements DataChangeListener {
	private static final long serialVersionUID = 1L;

	public enum Commands implements CtrlProvider {
		Active(JToggleButton.class), Execute(null);

		private Commands(Class<? extends JComponent> ctrlClass) {
			this.ctrlClass = ctrlClass;
		}

		Class<? extends JComponent> ctrlClass;

		@Override
		public Class<? extends JComponent> getCtrlClass() {
			return ctrlClass;
		}
	}

	CommandListener<Commands> cmdListener = new CommandListener<Commands>(Commands.class) {
		@Override
		void doCommand(Commands cmd, ActionEvent e) throws Exception {
			switch (cmd) {
			case Active:
				active = ((JToggleButton) e.getSource()).isSelected();
				getButton(Commands.Execute).setEnabled(!active);
				if ( pending ) {
					run();
				}
				break;

			case Execute:
				run();
				break;
			}
		}
	};

	public enum Area {
		// Input(IPFSwingToolsListSwitchPanel.class),
		// Edit(IPFSwingToolsContentPanel.class),
		// Output(IPFSwingToolsListSwitchPanel.class);
		Input(JTabbedPane.class), Edit(IPFSwingPanelDataEdit.class), Output(JTabbedPane.class);

		public final Class<? extends JComponent> areaClass;

		private Area(Class<? extends JComponent> areaClass) {
			this.areaClass = areaClass;
		}

		JComponent create() throws Exception {
			JComponent ret = areaClass.newInstance();
			// ret.init();
			// ret.setHead(name());
			return ret;
		}
	}

	EnumMap<Area, JComponent> areas = new EnumMap<>(Area.class);

	JSplitPane splOutCenter;
	JSplitPane splInEdit;

	ProcessContext pctx;
	DataObject algType;

	DataObject algorithm;
	boolean pending;
	boolean active;

	public IPFSwingPanelAlgorithm(DataObject ob, boolean type) {
		super(GuiElements.PanelAlgorithm);

		try {

			if (type) {
				this.algType = ob;
				algorithm = IPFUtilsCommon.createObjectByType(ob);
			} else {
				algorithm = ob;
				this.algType = algorithm.getAttribute(SPEC_FLD_TYPEOB);
			}

			algorithm.setListener(ListenerOp.add, this);

			pctx = new IPFUtilsContext();
		} catch (Exception e) {
		}
	}

	@Override
	protected JComponent getContent() throws Exception {
		for (Area a : Area.values()) {
			areas.put(a, a.create());
		}

		if (null != algType) {
			DataObject p = algType.getAttribute("Parameters");
			((IPFSwingPanelDataEdit) areas.get(Area.Edit)).init(p, algorithm, true);

			initSideArea(Area.Input, algType);
			initSideArea(Area.Output, algType);
		}

//		run();

		splInEdit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splOutCenter = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

		splInEdit.setLeftComponent(areas.get(Area.Input));
		splInEdit.setRightComponent(areas.get(Area.Edit));
		splInEdit.setDividerLocation(0.5);
		splInEdit.setResizeWeight(0.5);

		splOutCenter.setLeftComponent(splInEdit);
		splOutCenter.setRightComponent(areas.get(Area.Output));
		splOutCenter.setDividerLocation(0.5);
		splOutCenter.setResizeWeight(0.5);

		// IPFSwingToolsSaverPanel sp = new IPFSwingToolsSaverPanel();
		//
		 JPanel ret = new JPanel(new BorderLayout());
		 ret.add(cmdListener.getPanel(true), BorderLayout.SOUTH);
		 ret.add(splOutCenter, BorderLayout.CENTER);
		
		 return ret;
//		return splOutCenter;
	}

	@Override
	public void dataChanged(DataChangeEvent event) {
		if (algorithm == event.getObject()) {
			if (active) {
				run();
			} else {
				pending = true;
			}
		}
	}

	JTabbedPane initSideArea(Area area, DataObject type) {
		DataObject p = algType.getAttribute(area);

		if (null == p) {
			return null;
		}

		JTabbedPane tp = (JTabbedPane) areas.get(area);

		tp.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);

		for (String n : p.getAttributeNames()) {
			tp.add(n, new IPFSwingToolsImagePanel());
		}

		return tp;
	}

	void setImage(Area area, int tab, BufferedImage image) {
		JTabbedPane tp = (JTabbedPane) areas.get(area);

		if (tp.getComponentCount() > tab) {
			IPFSwingToolsImagePanel ip = (IPFSwingToolsImagePanel) tp.getComponentAt(tab);
			ip.setImage(image);
		}
	}

	protected void run() {
		try {
			IPFUtilsImageSerializer is = new IPFUtilsImageSerializer();

			BufferedImage img = ImageIO.read(new File(defaultImage));

			setImage(Area.Input, 0, img);

			is.setImg(img);
			pctx.writeStream("Image", is);

			DataObject msg = IPFUtilsCommon.createDataObject("msg");

			msg.setAttribute(MessageFields.Context, pctx);

			IPFUtilsCommon.send(algorithm, msg);

			pctx.readStream("Image", is, null);
			BufferedImage tImg = is.getImg();

			setImage(Area.Output, 0, tImg);
		} catch (Throwable t) {
			t.printStackTrace(System.err);
		}
		pending = false;
	}
}
