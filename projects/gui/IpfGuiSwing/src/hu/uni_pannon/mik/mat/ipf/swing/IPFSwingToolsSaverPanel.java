package hu.uni_pannon.mik.mat.ipf.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.swing.IPFSwingComponents.CommandListener;

class IPFSwingToolsSaverPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final String ATT_SAVEINFO = "SaveInfo";
	private static final String ATT_FILENAME = "fileName";
	private static final String ATT_LASTUPDATE = "lastUpdate";
	private static final String ATT_DESCRIPTION = "description";

	DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);

	enum Command {
		Save;
	}

	JTextField tfName;
	JLabel lbLastSaved;

	JTextArea taDescription;

	DataObject ob;

	CommandListener<Command> cmdListener = new CommandListener<Command>(Command.class) {
		@Override
		void doCommand(Command cmd, ActionEvent e) throws Exception {
			String fName = tfName.getText();
			String err = null;
			int type = JOptionPane.ERROR_MESSAGE;
			File f;

			if (IPFUtilsBasic.strIsEmpty(fName)) {
				err = "The file name is empty!";
			} else {
				f = new File(fName);
				if (f.isDirectory()) {
					err = "The name points to a directory!";
				} else if (f.exists()) {
					err = "The file exists! Overwrite?";
					type = JOptionPane.YES_NO_OPTION;
				}
			}

			int ret = -1;

			if (null != err) {
				if (JOptionPane.ERROR_MESSAGE == type) {
					JOptionPane.showMessageDialog(IPFSwingToolsSaverPanel.this, err, "Saving...", type);
				} else {
					ret = JOptionPane.showConfirmDialog(IPFSwingToolsSaverPanel.this, err, "Saving...", type);
				}

				if ((JOptionPane.ERROR_MESSAGE == type) || (JOptionPane.NO_OPTION == ret)) {
					return;
				}
			}
			
			DataObject si = getSaveInfo(true);

			si.setAttribute(ATT_DESCRIPTION, taDescription.getText());
			si.setAttribute(ATT_FILENAME, fName);
			si.setAttribute(ATT_LASTUPDATE, df.format(new Date()));

			IPFUtilsCommon.store(ob, fName);

			update();
		}
	};

	IPFSwingToolsSaverPanel() {
		super(new BorderLayout());

		tfName = new JTextField();
		lbLastSaved = new JLabel();

		JPanel actions = new JPanel(new GridLayout(3, 1));
		actions.add(tfName);
		actions.add(lbLastSaved);
		actions.add(cmdListener.getButton(Command.Save));

		actions.setPreferredSize(new Dimension(200, 60));

		add(actions, BorderLayout.WEST);

		taDescription = new JTextArea();

		tfName.setText("<Enter name>");
		lbLastSaved.setText("Not saved yet.");
		taDescription.setText("<Enter description>");
		tfName.selectAll();
		taDescription.selectAll();

		add(new JScrollPane(taDescription), BorderLayout.CENTER);

		setBorder(new TitledBorder(new LineBorder(Color.black), "Keep this object"));
	}

	void setObject(DataObject ob) {
		this.ob = ob;
	}

	DataObject getSaveInfo(boolean createIfMissing) {
		DataObject saveInfo = ob.getAttribute(ATT_SAVEINFO);
		if (createIfMissing && (null == saveInfo)) {
			saveInfo = IPFUtilsCommon.createDataObject(null);
			ob.setAttribute(ATT_SAVEINFO, saveInfo);
		}
		
		return saveInfo;
	}

	void update() {
		DataObject saveInfo = getSaveInfo(false);
		
		if (null != saveInfo) {
			tfName.setText(saveInfo.getAttribute(ATT_FILENAME));
			tfName.setText(saveInfo.getAttribute(ATT_LASTUPDATE));
			tfName.setText(saveInfo.getAttribute(ATT_DESCRIPTION));
		}
	}
}