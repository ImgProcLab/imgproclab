package hu.uni_pannon.mik.mat.ipf.runtime;

import java.util.HashMap;
import java.util.Map;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.CloudEvents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.CloudMessages;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.LogicFactory;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.LogicFactoryFields;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.CmdCall;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.Logic;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.LogicDefault;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public class IPFRuntimeLogicFactory extends LogicDefault implements IPFRuntimeComponents, LogicFactory {

	class PackageInfo {
		@SuppressWarnings("unchecked")
		Class<? extends Logic> classForName(String className) {
			try {
				return (Class<? extends Logic>) Class.forName(className);
			} catch (Throwable e) {
				IPFUtilsCommon.safeSwallowException(e, CloudEvents.invalidLogicClass, true);
				return null; // fake return
			}
		};
	}

	class ClassInfo extends DataObjectWrapper {
		PackageInfo pi;

		Class<? extends Logic> lc;

		public ClassInfo(DataObject from) {
			super(from);
		}

		@SuppressWarnings("unchecked")
		synchronized <T extends Logic> T createLogic() {
			if (null == pi) {
				String pn = wrapped.getAttribute(LogicFactoryFields.packageId);
				pi = packages.get(pn);
			}
			if (null == lc) {
				String className = wrapped.getAttribute(LogicFactoryFields.implClass);
				lc = pi.classForName(className);
			}
			try {
				return (T) lc.newInstance();
			} catch (Throwable e) {
				IPFUtilsCommon.safeSwallowException(e, CloudEvents.invalidLogicClass, true);
				return null; // fake return
			}
		}
	}

	Map<String, PackageInfo> packages = new HashMap<String, PackageInfo>();
	Map<String, ClassInfo> classes = new HashMap<String, ClassInfo>();

	public IPFRuntimeLogicFactory() {
		super(CloudMessages.class);
		packages.put(null, new PackageInfo());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void loadConfig(DataObject config) throws Exception {
		config.iterateAttribute(LogicFactoryFields.LogicAssignments, true, new DataProcessorDefault<DataObject>() {
			@Override
			public ProcessResult processData(Object key, DataObject data) throws Exception {
				ClassInfo ci = new ClassInfo(data);
				classes.put(ci.getId(), ci);
				return ProcessResult.processed;
			}
		});
	}

	@Override
	protected ProcessResult process(CmdCall cmd, DataObject param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Logic createLogic(Object type) throws Exception {
		return classes.get(IPFUtilsBasic.safeToStr(type)).createLogic();
	}
}
