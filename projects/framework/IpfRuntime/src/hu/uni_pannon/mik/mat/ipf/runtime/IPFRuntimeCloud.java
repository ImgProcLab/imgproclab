package hu.uni_pannon.mik.mat.ipf.runtime;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.Logic;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.LogicWrapper;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsPool;

public class IPFRuntimeCloud implements IPFSharedCloud, IPFRuntimeComponents {
	DataObjectFactory dataFactory;
	LogicFactory logicFactory;

	Map<String, DataObject> rootBeans = new HashMap<String, DataObject>();

	IPFUtilsPool<DataObject, Logic> smartBeans = new IPFUtilsPool<DataObject, Logic>(IPFUtilsPool.PoolType.weak, CloudEvents.logicCreationException, NO_LOGIC) {
		@Override
		protected Logic create(DataObject key) throws Exception {
			Logic l = null;
			String type = key.getAttribute(SPEC_FLD_TYPE);

			if (null != type) {
				l = logicFactory.createLogic(type);
				if (null != l) {
					l.init(key);
				}
			}

			return (null == l) ? NO_LOGIC : l;
		}
	};

	@Override
	public void init(DataObjectFactory dataFactory, LogicFactory logicFactory) {
		this.dataFactory = dataFactory;
		this.logicFactory = logicFactory;
	}

	@Override
	public DataObject createObject(Object type) {
		return dataFactory.createObject(type);
	}
	
	@Override
	public DataObject load(Object source) throws Exception {
		DataObject data = dataFactory.load(source);
		
		Set<DataObject> seen = new HashSet<>();
		
		data.browseTree(seen, new DataProcessorDefault<DataObject>() {
			@Override
			public ProcessResult processData(Object key, DataObject data) throws Exception {
				ProcessResult ret = ProcessResult.skipped;
				
				String id = data.getAttribute(SPEC_FLD_ID);
				if (null != id) {
					rootBeans.put(id, data);
					ret = ProcessResult.processed;
				}
				
				return ret;
			}
		});

		return data;
	}
	
	@Override
	public void store(DataObject ob, Object source) throws Exception {
		dataFactory.save(source, ob);
	}

	@Override
	public DataObject getObject(Object id) {
		return rootBeans.get(id);
	}
	
	@Override
	public ProcessResult iterateByType(Object type, DataProcessor<DataObject> processor) throws Exception {
		boolean init = true;
		ProcessResult ret = ProcessResult.skipped;
		
		for ( DataObject o : rootBeans.values() ) {
			if ( IPFUtilsBasic.safeIsEqual(type, o.getAttribute(SPEC_FLD_TYPE))) {
				if ( init ) {
					processor.processStart(type);
				}
				if ( (ret = processor.processData(type, o)).stop ) {
					break;
				}
			}
		}
		
		if ( !init ) {
			processor.processEnd(type, ret);
		}
		
		return ret;
	}

	@Override
	public ProcessResult send(DataObject target, DataObject message) throws Exception {
		Logic l = smartBeans.get(target);
		if (null != message) {
			return l.process(message);
		}
		return ProcessResult.skipped;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getWrappedObject(DataObject ob) {
		T ret = null;

		try {
			// TODO nice to have: check friendship declarations, some time later

			Object l = smartBeans.get(ob);
			ret = (l instanceof LogicWrapper) ? ((LogicWrapper<T>) l).getWrapped() : null;

		} catch (Throwable t) {
			IPFUtilsCommon.safeSwallowException(t, CloudEvents.friendAccessFailed, false);
		}

		return ret;
	}
}
