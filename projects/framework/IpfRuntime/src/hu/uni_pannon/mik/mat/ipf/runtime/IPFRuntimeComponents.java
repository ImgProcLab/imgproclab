package hu.uni_pannon.mik.mat.ipf.runtime;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents;

public interface IPFRuntimeComponents extends IPFSharedComponents, IPFSharedDataComponents {
	String STEP_SEP = "_";
	
	interface RuntimeContext extends ProcessContext {
		void nextStep();
	}
}
