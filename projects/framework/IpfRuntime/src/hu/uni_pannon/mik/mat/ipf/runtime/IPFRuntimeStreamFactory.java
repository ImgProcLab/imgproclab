package hu.uni_pannon.mik.mat.ipf.runtime;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.StreamFactory;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.StreamFactoryMessages;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.CmdCall;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.LogicDefault;

public class IPFRuntimeStreamFactory extends LogicDefault implements StreamFactory, IPFRuntimeComponents {

	public IPFRuntimeStreamFactory() {
		super(StreamFactoryMessages.class);
	}
	
	@Override
	public void loadConfig(DataObject config) {
		
	}
		
	@Override
	public void removeStream(String id, int step) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public InputStream getInputStream(String id, int step) throws Exception {
		return new FileInputStream(id);
	}
	
	@Override
	public OutputStream getOutputStream(String id, int step) throws Exception {
		return new FileOutputStream(id);
	}

	@Override
	protected ProcessResult process(CmdCall cmd, DataObject param) throws Exception {
		String name = param.getAttribute(GenericFields.name);
		Object result = null;
		ProcessResult ret = ProcessResult.failed;
		
		switch ((StreamFactoryMessages) cmd.getCmd()) {
		case getInputStream:
			result = this.getClass().getClassLoader().getResourceAsStream(name);
			break;

		default:
			break;
		}

		if ( null != result ) {
			ret = ProcessResult.processed;
			param.setAttribute(GenericFields.result, result);
		}
		
		return ret ;
	}

}
