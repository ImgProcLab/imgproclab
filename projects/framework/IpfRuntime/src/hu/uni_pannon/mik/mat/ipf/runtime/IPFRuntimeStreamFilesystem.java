package hu.uni_pannon.mik.mat.ipf.runtime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.StreamFactory;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.StreamFactoryMessages;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.CmdCall;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedLogic.LogicDefault;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsBasic;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;

public class IPFRuntimeStreamFilesystem extends LogicDefault implements StreamFactory, IPFRuntimeComponents {
	File folder;

	public IPFRuntimeStreamFilesystem() {
		super(StreamFactoryMessages.class);
	}

	@Override
	public void loadConfig(DataObject config) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(DataObject self) throws Exception {
		super.init(self);

		String parentId = IPFUtilsCommon.getAttribute(self, GenericFields.Parent, GenericFields.id);
		String relPath = IPFUtilsCommon.getAttribute(self, GenericFields.path);

		String myPath = IPFUtilsBasic.buildStr(File.pathSeparator, parentId, relPath);

		folder = new File(myPath);
		folder.mkdirs();
		self.setAttribute(GenericFields.id, folder.getAbsolutePath());
	}

	public File getFile(String id, int step) throws Exception {
		return new File(folder, IPFUtilsBasic.buildStr(STEP_SEP, id, step));
	}

	@Override
	public void removeStream(String id, int step) throws Exception {
		FileOutputStream os = new FileOutputStream(getFile(id, step));
		os.close();
	}
	
	@Override
	public InputStream getInputStream(final String id, final int step) throws Exception {
		File last = null;

		for (File f : folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				int sep = name.lastIndexOf(STEP_SEP);
				try {
					return (-1 != sep) && id.equals(name.substring(0, sep)) && (step >= Integer.valueOf(name.substring(sep + 1)));
				} catch (Throwable t) {
					return false;
				}
			}
		})) {
			if ((null == last) || (0 < last.getName().compareTo(f.getName()))) {
				last = f;
			}
		}
		return ((null == last) || (0 == last.length())) ? null : new FileInputStream(last);
	}

	@Override
	public OutputStream getOutputStream(String id, int step) throws Exception {
		return new FileOutputStream(getFile(id, step));
	}

	@Override
	protected ProcessResult process(CmdCall cmd, DataObject param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
