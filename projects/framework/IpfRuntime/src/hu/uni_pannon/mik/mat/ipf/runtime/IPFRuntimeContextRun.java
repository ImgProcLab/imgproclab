package hu.uni_pannon.mik.mat.ipf.runtime;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.TreeSet;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.DataObjectFactory;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.StreamFactory;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsPool;

public class IPFRuntimeContextRun implements IPFRuntimeComponents, IPFRuntimeComponents.RuntimeContext {

	StreamFactory streamFactory;
	DataObjectFactory objectFactory;
	
	IPFUtilsPool<Integer, Set<String>> index = new IPFUtilsPool<Integer, Set<String>>() {
		@Override
		protected Set<String> create(Integer key) throws Exception {
			return new TreeSet<>();
		}
	};
	int currStep;

	@Override
	public Integer getCurrentStep() {
		return currStep;
	}
	
	@Override
	public void nextStep() {
		Set<String> prevSet = index.get(currStep);
		++currStep;
		if ( null != prevSet ) {
			index.get(currStep).addAll(prevSet);
		}
	}
	
	void addId(String id) {
		index.get(currStep).add(id);
	}
	
	@Override
	public Iterable<String> getIds(boolean includePrevious, Integer step) {
		Set<String> ret = index.peek(step);
		
		if ( includePrevious && (0 < step)) {
			ret = new TreeSet<>(ret);
			Set<String> ss;
			for ( int i = 0; i < step; ++i ) {
				ss = index.peek(i);
				if ( null != ss ) {
					ret.addAll(ss);
				}
			}
		}		
		
		return ret;
	}
	
	@Override
	public void readStream(String id, StreamReader reader, Integer step) throws Exception {
		InputStream is = null;
		try {
			is = streamFactory.getInputStream(id, (null == step) ? currStep : step);
			if (null != is) {
				reader.loadStream(is);
			}
		} finally {
			if (null != is) {
				is.close();
			}
		}
	}

	@Override
	public void writeStream(String id, StreamWriter writer) throws Exception {
		OutputStream os = null;
		try {
			os = streamFactory.getOutputStream(id, currStep);
			writer.writeStream(os);
		} finally {
			if (null != os) {
				os.close();
			}
		}
	}

	@Override
	public DataObject getData(String id, Integer step) throws Exception {
		InputStream is = null;
		try {
			is = streamFactory.getInputStream(id, (null == step) ? currStep : step);
			if (null != is) {
				return objectFactory.load(is);
			}
		} finally {
			if (null != is) {
				is.close();
			}
		}
		return null;
	}

	@Override
	public void setData(String id, DataObject data) throws Exception {
		OutputStream os = null;
		try {
			os = streamFactory.getOutputStream(id, currStep);
			objectFactory.save(os, data);
		} finally {
			if (null != os) {
				os.close();
			}
		}
	}

	@Override
	public void remove(String id) throws Exception {
		streamFactory.removeStream(id, currStep);
		Set<String> i = index.peek(currStep);
		if ( null != i) {
			i.remove(id);
		}
	}
}
