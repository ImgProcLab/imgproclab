package hu.uni_pannon.mik.mat.ipf.frame.json;

import hu.uni_pannon.mik.mat.ipf.shared.*;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedCloud.StreamFactory;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;

import java.io.*;
import java.nio.charset.Charset;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class IPFFrmJsonDataFactory implements IPFFrmJsonComponents, IPFSharedCloud.DataObjectFactory {
	JSONParser jp = new JSONParser();

	StreamFactory streamFactory;

	public IPFFrmJsonDataObject fromFile(String fileName) throws Exception {
		synchronized (jp) {
			InputStream is = streamFactory.getInputStream(fileName, 0);
			try {
				return new IPFFrmJsonDataObject((JSONObject) jp.parse(new InputStreamReader(is)));
			} finally {
				is.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataObject createObject(Object type) {
		JSONObject o = new JSONObject();
		o.put(SPEC_FLD_TYPE, type);
		return new IPFFrmJsonDataObject(o);
	}

	@Override
	public void setSource(Object source) {
		streamFactory = (StreamFactory) source;
	}

	@Override
	public DataObject load(Object source) throws Exception {
		return fromFile((String) source);
	}

	@Override
	public void save(Object targetId, DataObject data) throws Exception {
		OutputStream os = streamFactory.getOutputStream((String)targetId, 0);
		os.write(data.toString().getBytes("UTF8"));
		os.flush();
		os.close();
	}

	@Override
	public DataObject load(InputStream is) throws Exception {
		return new IPFFrmJsonDataObject((JSONObject) jp.parse(new InputStreamReader(is)));
	}

	@Override
	public void save(OutputStream os, DataObject data) throws Exception {
		OutputStreamWriter ow = new OutputStreamWriter(os);
		((IPFFrmJsonDataObject) data).object.writeJSONString(ow);
		ow.close();
	}
}
