package hu.uni_pannon.mik.mat.ipf.frame.json;

import java.util.HashSet;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.AttributeType;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataChangeEvent;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataChangeListener;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataObject;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.DataProcessor;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.ListenerOp;
import hu.uni_pannon.mik.mat.ipf.shared.IPFSharedDataComponents.ValueType;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsCommon;
import hu.uni_pannon.mik.mat.ipf.shared.utils.IPFUtilsPool;

@SuppressWarnings("unchecked")
public class IPFFrmJsonDataObject implements IPFFrmJsonComponents, IPFSharedDataComponents.DataObject {
	class DataChangeEventImpl implements DataChangeEvent {
		String name;

		public DataChangeEventImpl(String name) {
			this.name = name;
		}

		@Override
		public DataObject getObject() {
			return IPFFrmJsonDataObject.this;
		}

		@Override
		public String getAttrName() {
			return name;
		}
		
	}

	JSONObject object;
	IPFFrmJsonDataObject root;
	IPFUtilsPool<JSONObject, IPFFrmJsonDataObject> poolMembers = new IPFUtilsPool<JSONObject, IPFFrmJsonDataObject>() {
		@Override
		protected IPFFrmJsonDataObject create(JSONObject key) {
			return new IPFFrmJsonDataObject(key, root);
		}
	};
	
	Set<DataChangeListener> listeners;

	<T> T optPoolMembers(Object o) {
		return (T) ((o instanceof JSONObject) ? root.poolMembers.get((JSONObject) o) : o);
	}

	IPFFrmJsonDataObject(JSONObject ob, IPFFrmJsonDataObject root) {
		this.object = ob;
		this.root = root;
	}

	IPFFrmJsonDataObject(JSONObject ob) {
		this.object = ob;
		this.root = this;
	}

	@Override
	public String toString() {
		return object.toString();
	}

	@Override
	public String getTypeName() {
		return (String) object.get(SPEC_FLD_TYPE);
	}

	@Override
	public Iterable<String> getAttributeNames() {
		return object.keySet();
	}

	@Override
	public AttributeType getAttributeType(Object key) {
		Object o = object.get(key);
		if ( null == o ) {
			return AttributeType.attTypeNull;
		} else if ( o instanceof JSONObject ) {
			return AttributeType.attTypeObject;
		}else if ( o instanceof JSONArray ) {
			return AttributeType.attTypeArray;
		}else  {
			return AttributeType.attTypeValue;
		}
	}
	
	@Override
	public ValueType getValueType(Object key) {
		return ValueType.fromObject(object.get(key));
	}
	
	@Override
	public <T> T getAttribute(Object key) {
		return (T) optPoolMembers(object.get(IPFUtilsCommon.toStrKey(key)));
	}

	public <T> ProcessResult processArray(Object key, JSONArray arr, int dpIdx, boolean skipNulls,
			DataProcessor<T>... dp) throws Exception {
		ProcessResult ret = ProcessResult.skipped;
		if (null != arr) {
			boolean init = true;
			DataProcessor<T> proc = dp[dpIdx];
			for (Object o : arr) {
				if (skipNulls && (null == o)) {
					continue;
				}
				if (init && (null != proc)) {
					proc.processStart(key);
					init = false;
				}

				if (o instanceof JSONArray) {
					ret = processArray(key, (JSONArray) o, dpIdx + 1, skipNulls, dp);
				} else {
					if (null != proc) {
						ret = proc.processData(key, (T) optPoolMembers(o));
						if (ret.stop) {
							break;
						}
					}
				}
			}

			if (!init) {
				proc.processEnd(key, ret);
			}
		}
		return ret;
	}

	boolean optBrowseArray(Object optArr, Set<DataObject> seen, DataProcessor<DataObject> dp) throws Exception {
		if (optArr instanceof JSONArray) {
			for (Object o1 : (JSONArray) optArr) {
				if (!optBrowseArray(o1, seen, dp)) {
					o1 = optPoolMembers(o1);
					if (o1 instanceof DataObject) {
						((IPFFrmJsonDataObject) o1).browseTree(seen, dp);
					}
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public <T> ProcessResult browseTree(Set<DataObject> seen, DataProcessor<DataObject> dp) throws Exception {
		ProcessResult ret = ProcessResult.skipped;

		if (seen.add(this)) {
			dp.processData(null, this);
			for (String s : getAttributeNames()) {
				Object o = getAttribute(s);

				if ( !optBrowseArray(o, seen, dp)) {
					if ( o instanceof DataObject ) {
						((DataObject)o).browseTree(seen, dp);
					}
				}
			}
			ret = ProcessResult.processed;
		}

		return ret;
	}

	@Override
	public <T> ProcessResult iterateAttribute(Object key, boolean skipNulls, DataProcessor<T>... dp) throws Exception {
		String name = IPFUtilsCommon.toStrKey(key);
		return processArray(key, (JSONArray) object.get(name), 0, skipNulls, dp);
	}

	@Override
	public void removeAttribute(Object key, int... index) {
		object.remove(key);
	}

	@Override
	public void setAttribute(Object key, Object value) {
		String name = IPFUtilsCommon.toStrKey(key);
		object.put(name, value);
		
		if ( null != listeners ) {
			DataChangeEventImpl i = new DataChangeEventImpl(name);
			for ( DataChangeListener l : listeners ) {
				l.dataChanged(i);
			}
		}
	}

	@Override
	public void setAttribute(Object key, int index, Object... value) {
		if (IPFUtilsCommon.isArrOK(value)) {
			String name = IPFUtilsCommon.toStrKey(key);
			JSONArray arr = (JSONArray) object.get(name);
			if ( null == arr ) {
				arr = new JSONArray();
				object.put(key, arr);
			}
			for (Object v : value) {
				arr.add(index++, v);
			}
		}
	}
	
	@Override
	public boolean setListener(ListenerOp op, DataChangeListener listener) {
		switch (op) {
		case add:
			if ( null == listeners ) {
				listeners = new HashSet<>();
			}
			return listeners.add(listener);
		case clear:
			if ( null != listeners ) {
				listeners.clear();
				return true;
			}
			return false;
		case remove:
			return ( null == listeners ) ? false : listeners.remove(listener);
		case test:
			return ( null == listeners ) ? false : listeners.contains(listener);
		default:
			break;
		
		}
		return false;
	}
}
