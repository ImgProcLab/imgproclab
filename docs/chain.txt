A szoftver a PE MIK Képfeldolgozás Kutatólaboratórium számára készül, célja a laboratóriumban folyó kutatások támogatása. A könnyen bővíthető keretrendszer segítségével vizsgálni lehet egy-egy képen egy adott probléma megoldásához több (és többféleképpen paraméterezett) algoritmusláncot, majd a megfelelő láncot batch módban sok (adott mappába gyűjtött) képen tudjuk alkalmazni.

Példa: objektumfelismerés valós körülmények (változó fényviszonyok, zajok) között

Lánc: zajszűrés -> színi torzítás kezelése -> képi jellemzők kivonása -> képi jellemzők összehasonlítása -> eredmény értékelése

A lánc minden lépése többféle algoritmussal és különféle paraméterekkel megoldható. A lánc elemei kicserélhetőek, paraméterezhetőek, és az eredmény tükrében a legjobban működő kiválasztható.

Fontos, hogy egy keretrendszerből és több modulból álljon a program, ami újrafordítás nélkül bővíthető újabb algoritmusokat tartalmazó modulokkal.

----

Chain: Denoising -> Chromatic aberration correction -> Feature detection -> Feature description -> Feature matching -> Evaluation

A possible chain for demonstration: Non-local means denoising -> ORB feature detection -> ORB feature description -> Brute force matching of features on transformed image or sequence -> Evaluation of ORB feature detector by prior knowledge of feature locations

----

Denoising

Denoising aims to reduce the noise inherently present in images due to the physical limitations of camera sensors, media, or processing.

There are several different types of image noise. Gaussian noise, salt and pepper noise, photon shot or poisson noise, quantization noise are the most notable examples.

----

Denoising algorithms in OpenCV

http://docs.opencv.org/modules/photo/doc/denoising.html

OpenCV supports the Non-Local Means algorithm.

This algorithm removes noise in a patch of pixels by searching for similar patches and calculating their weighted average, with the weights coming from the euclidean distance of the pixel values between the two patches.

----

Chromatic aberration correction

This step is ordinarily done on hardware, because the exact chromatic aberration is specific to camera lens.

OpenCV does not have any algorithms specifically to address chromatic aberration, nor do I have knowledge of any well-known algorithm for this purpose.

This topic needs further research.

----

Feature detection

Corners, features, interest points, and keypoints are used interchangably in the literature. For the sake of simplicity and generality, the terminology "features" is used from now on.

Feature detection aims at finding characteristic or "interesting" local neighborhood patterns on an image. Ideally, features should be invariant to transformations such as scaling, translation, rotation, perspective change, and lighting. An ideal feature detector should detect the exact same features when the image is modified in any such a manner.

Found features are candidates for extraction as feature descriptors which can then be used to compare features on other images. They are used in applications such as object tracking, object recognition, face recognition, image fingerprinting, image stitching, and others.

Feature description algorithms often come with their own feature detector algorithm, but this is not always the case. FREAK for example is only a feature descriptor without an associated feature detector. It is possible to combine different detectors and descriptors.

----

Feature detection algorithms in OpenCV

http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_feature_detectors.html#featuredetector-create
http://computer-vision-talks.com/articles/2011-01-04-comparison-of-the-opencv-feature-detection-algorithms/
http://computer-vision-talks.com/articles/2011-07-13-comparison-of-the-opencv-feature-detection-algorithms/

Feature detector algorithms supported by OpenCV are FAST, STAR, SIFT, SURF, ORB, BRISK, MSER, GFTT, HARRIS, Dense and SimpleBlob.

FAST is very fast, as its name indicates. However it is not robust, it detects too many features, a lot of which are noise.

STAR and MSER are too sensitive to brightness changes.

SIFT and SURF are patented algorithms, their use is not recommended.

ORB is especially suited for tracking, it shows the smallest drift of tracked features.

----

Feature descriptors

Feature descriptors are vectors or bitstrings that distinctly describe the feature, or the pattern of local neighborhood of a point. Ideal feature descriptors are invariant to scaling, translation, rotation, perspective change, and lighting.

Feature descriptors could range from something as simple as a rectangular cutout from an image, to more complex models based on gaussian structures and the human retina.

Feature descriptors can be encoded as vectors or as bitstrings. Binary descriptors and shorter descriptors have more favorable data storage properties.

----

Feature descriptor algorithms in OpenCV

http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_extractors.html#descriptorextractor-create
http://computer-vision-talks.com/articles/2011-01-28-comparison-of-feature-descriptors/
http://computer-vision-talks.com/articles/2012-08-18-a-battle-of-three-descriptors-surf-freak-and-brisk/

Feature descriptors supported by OpenCV are SIFT, SURF, BRIEF, BRISK, ORB, and FREAK.

SIFT and SURF are patented algorithms, their use is not recommended.

BRIEF is neither scale nor rotation invariant, it should be avoided.

ORB uses binary feature descriptors.

FREAK is a short binary feature descriptor based on a model of the human retina.

----

Feature matching

Feature matching tries to create a correspondence of features between two images, taking account the spatial structure of the feature sets.

----

Feature matching algorithms in OpenCV

http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_matchers.html#descriptormatcher-create
http://docs.opencv.org/modules/flann/doc/flann_fast_approximate_nearest_neighbor_search.html

Feature matching algorithms supported by OpenCV are brute force matching by various norms, and various FLANN matchers.

----

Evaluation

Evaluation of feature detectors and descriptors is possible in a similar manner to the computer-vision-talks.com performance tests.

First a reference image should be detected and extracted for features. Then these features should be matched to the features detected on scaled, translated, rotated, perspective changed, lighting changed, occluded, or sequentially related images.

With prior knowledge of the exact change in the images, we accurately know the feature positions on the transformed images. With that information, we can easily evaluate how accurately the feature detector recognizes the same features on the transformed images.

----

Other information

http://dsp.stackexchange.com/questions/10423/why-do-we-use-keypoint-descriptors
http://stackoverflow.com/questions/19540138/which-feature-detector-algorithm-is-simplest-for-learning
http://stackoverflow.com/questions/29133085/what-are-keypoints-in-image-processing
